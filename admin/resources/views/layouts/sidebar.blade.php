<?php
if (Auth::user()) {
    ?>
    <div class="navbar-default sidebar" role="navigation">

        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <!--            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <input type="text" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="button">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                                 /input-group 
                            </li>-->
                <li>
                    <a href="{{ url('/home') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>
                <?php
                if (Auth::user()->isAdmin()) {
                    ?>
                    <li>
                        <a href="{{ url('level/') }}"><i class="fa fa-level-up"></i> Level</a>
                    </li>
                    <li>
                        <a href="{{ url('parent/') }}"><i class="fa fa-child"></i> Parents</a>
                    </li>
                    <li>
                        <a href="{{ url('teacher/') }}"><i class="fas fa-chalkboard-teacher"></i> Teacher</a>
                    </li>
                    <li>
                        <a href="{{ url('karyakarta/') }}"><i class="fa fa-group"></i> Karyakarta</a>
                    </li>
                    <li>
                        <a href="{{ url('news/') }}"><i class="fa fa-newspaper-o"></i> News</a>
                    </li>
                     <li>
                        <a href="{{ url('home-page-slider/') }}"><i class="fa fa-sliders"></i> Home Page Slider</a>
                    </li>
                    <li>
                        <a href="{{ url('transaction/') }}"><i class="fa fa-money"></i> Transactions</a>
                    </li>
                    <li>
                        <a href="{{ url('latest-updates/') }}"><i class="fa fa-refresh"></i> Latest Updates</a>
                    </li>
                     <li>
                        <a href="{{ url('syllabus-calender/') }}"><i class="fa fa-calendar-o"></i> Syllabus Calender</a>
                    </li>
                     <li>
                        <a href="{{ url('academic-calender/') }}"><i class="fa fa-calendar-o"></i> Academic Calender</a>
                    </li>
                    <li>
                        <a href="{{ url('event/') }}"><i class="fa fa-calendar"></i> Events</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-picture-o"></i> Gallery<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{ url('photo-gallery/') }}"><i class="fa fa-picture-o"></i> Photo Gallery</a>
                            </li>
                            <li>
                                <a href="{{ url('video-gallery/') }}"><i class="fa fa-file-video-o"></i> Video Gallery</a>
                            </li>
                        </ul>

                    </li>

                    <?php
                }
                ?>  

                <li>
                    <a href="{{ url('resources/') }}"><i class="fa fa-link"></i> Teaching Resources Link</a>
                </li>
                <li>
                    <a href="{{ url('assignment/') }}"><i class="fa fa-check-circle"></i> Assembly Assignment</a>
                </li>
                <li>
                    <a href="{{ url('library/') }}"><i class="fa fa-book fa-fw"></i> Balvihar Library</a>
                </li>

                <!--            <li>
                                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Charts<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="flot.html">Flot Charts</a>
                                    </li>
                                    <li>
                                        <a href="morris.html">Morris.js Charts</a>
                                    </li>
                                </ul>
                                 /.nav-second-level 
                            </li>
                            <li>
                                <a href="tables.html"><i class="fa fa-table fa-fw"></i> Tables</a>
                            </li>-->

            </ul>
        </div>
        <!-- /.sidebar-collapse -->

    </div>
    <?php
}
?>