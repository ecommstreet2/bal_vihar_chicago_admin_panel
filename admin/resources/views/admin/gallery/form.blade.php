<div class="col-md-4 {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>


<!--<div class="col-md-4">
    <div class=" {{ $errors->has('status') ? 'has-error' : ''}}">
        <label for="status" class="control-label">{{ 'Status' }}</label>
        {!! Form::select('status', ['1'=>'Active','0'=>'Inactive'],  null, ['class' => 'form-control']) !!}

    </div>

</div>-->

<div class="col-md-4" style="margin-top: 26px;">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
<div style="clear: both;margin-bottom: 50px;"></div>
