@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <div class="card-header"> <h4 style=""><span>Transaction #{{ $transaction->transaction_id }}</span></h4></div>
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">

                    <a href="{{ url('/transaction') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
<!--                    <a href="{{ url('/transaction/' . $transaction->id . '/edit') }}" title="Edit Transaction"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                    {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['transaction', $transaction->id],
                    'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-sm',
                    'title' => 'Delete Transaction',
                    'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                    {!! Form::close() !!}-->
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <th>ID</th><td>{{ $transaction->id }}</td>
                                </tr>
                                <tr><th> Transaction Id </th><td> {{ $transaction->transaction_id }} </td></tr>
                                <tr><th> Amount </th><td> {{ $transaction->amount }} </td></tr>
                                <tr><th> Status </th><td> {{ $transaction->status }} </td></tr>
                                <tr><th> Parent  </th><td> {{ $transaction->parent->fother_first_name ?? ''}} </td></tr><tr><th> Type </th><td> {{ $transaction->type }} </td></tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
