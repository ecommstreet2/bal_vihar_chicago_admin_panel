@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="card-header"> <h4 style=""><span>Latest Updates</span></h4></div>
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                    <a href="{{ url('/latest-updates/create') }}" class="btn btn-success btn-sm pull-left" title="Add New LatestUpdate">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>

                    {!! Form::open(['method' => 'GET', 'url' => '/latest-updates', 'class' => 'form-inline my-2 my-lg-0 float-right pull-right', 'role' => 'search'])  !!}

                    <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                    <span class="input-group-append">
                        <button class="btn btn-secondary" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>

                    {!! Form::close() !!}

                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th>#</th><th>Title</th><th>Description</th><th>Level</th><th>Date</th><th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($latestupdates as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>  {{ $item->title }} </td>
                                    <td>  {!! $item->description !!} </td>
                                    <td>  {{ $item->levels->name ?? '' }} </td>
                                    <td>{{ date('m/d/Y',strtotime($item->update_date)) }}</td>

                                    <td>
                                        <a href="{{ url('/latest-updates/' . $item->id) }}" title="View LatestUpdate"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> </button></a>
                                        <a href="{{ url('/latest-updates/' . $item->id . '/edit') }}" title="Edit LatestUpdate"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>
                                        {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => ['/latest-updates', $item->id],
                                        'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-sm',
                                        'title' => 'Delete LatestUpdate',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                        )) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $latestupdates->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
