<div class="col-md-6 {{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Title', ['class' => 'control-label']) !!}
    {!! Form::text('title', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-6 {{ $errors->has('update_date') ? 'has-error' : ''}}">
    {!! Form::label('update_date', 'Date', ['class' => 'control-label']) !!}
    {!! Form::text('update_date', null, ('' == 'required') ? ['class' => 'form-control date', 'required' => 'required'] : ['class' => 'form-control date']) !!}
    {!! $errors->first('update_date', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-6 {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
    {!! Form::textarea('description', null, ('' == 'required') ? ['class' => 'form-control ckeditor', 'required' => 'required'] : ['class' => 'form-control ckeditor']) !!}
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-6 {{ $errors->has('level') ? 'has-error' : ''}}">
    {!! Form::label('level', 'Level', ['class' => 'control-label']) !!}

    {!! Form::select('level', \DB::table("levels")->pluck("levels.name", "levels.id"),isset($latestupdate->level)?$latestupdate->level:null, ['class' => 'form-control','placeholder'=>'--Select Level---']) !!}

    {!! $errors->first('level', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-6">
    <div class=" {{ $errors->has('status') ? 'has-error' : ''}}">
        <label for="status" class="control-label">{{ 'Status' }}</label>
        {!! Form::select('status', ['1'=>'Active','0'=>'Inactive'], isset($latestupdate->status) ?$latestupdate->status : null, ['class' => 'form-control']) !!}
<!--            {!! $errors->first('status', '<p class="help-block">:message</p>') !!}-->
    </div>

</div>

<div class="col-md-12" style="margin-top: 10px;">
    <a href="{{ url('/latest-updates') }}" title="Back"><span class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</span></a>
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
<script type="text/javascript">
    $('.date').datepicker({
        format: 'mm/dd/yyyy'
    });
</script> 