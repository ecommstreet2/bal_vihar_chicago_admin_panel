<div class="col-md-12">
    <div class="col-md-6   {{ $errors->has('name') ? 'has-error' : ''}}">
        {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
        {!! Form::text('name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-md-6   {{ $errors->has('education') ? 'has-error' : ''}}">
        {!! Form::label('education', 'Education', ['class' => 'control-label']) !!}
        {!! Form::text('education', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('education', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-md-6   {{ $errors->has('designation') ? 'has-error' : ''}}">
        {!! Form::label('designation', 'Designation', ['class' => 'control-label']) !!}
        {!! Form::text('designation', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('designation', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="col-md-6   {{ $errors->has('phone') ? 'has-error' : ''}}">
        {!! Form::label('phone', 'Phone', ['class' => 'control-label']) !!}
        {!! Form::text('phone', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-md-6   {{ $errors->has('address') ? 'has-error' : ''}}">
        {!! Form::label('address', 'Address', ['class' => 'control-label']) !!}
        {!! Form::text('address', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-md-6   {{ $errors->has('email') ? 'has-error' : ''}}">
        {!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
        {!! Form::email('email', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-md-6   {{ $errors->has('image') ? 'has-error' : ''}}">
        {!! Form::label('image', 'Image', ['class' => 'control-label']) !!}
        {!! Form::file('image', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-md-6   {{ $errors->has('status') ? 'has-error' : ''}}">
        {!! Form::label('status', 'Status', ['class' => 'control-label']) !!}
        {!! Form::select('status', ['1'=>'Active','0'=>'Inactive'], isset($karyakarta->status) ?$karyakarta->status : null, ['class' => 'form-control']) !!}

        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-md-6">
        <?php
        if (!empty($karyakarta->image) &&  @getimagesize($karyakarta->image)) {
            ?>
            <img src="{{ $karyakarta->image }}" style="width: 100px;height: auto;"/>
            <?php
        }
        ?>
    </div>

    <div class="col-md-7   {{ $errors->has('description') ? 'has-error' : ''}}">
        {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
        {!! Form::textarea('description', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>


</div>
<div class="col-md-12" style="margin-top: 10px;margin-left: 14px;">
  <a href="{{ url('/karyakarta') }}" title="Back"><span class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</span></a>
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}

</div>