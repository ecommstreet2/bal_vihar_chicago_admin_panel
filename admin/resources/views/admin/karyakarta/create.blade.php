@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="card-header"> <h4 style=""><span>Create New Karyakarta</span></h4></div>
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                  
<!--                    <br />
                    <br />-->

                    <!--                    @if ($errors->any())
                                        <ul class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                        @endif-->

                    {!! Form::open(['url' => '/karyakarta', 'class' => 'form-horizontal', 'files' => true]) !!}

                    @include ('admin.karyakarta.form', ['formMode' => 'create'])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
