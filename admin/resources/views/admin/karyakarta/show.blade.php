@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <div class="card-header"> <h4 style=""><span> Karyakarta {{ $karyakarta->name }}</span></h4></div>
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">

                    <a href="{{ url('/karyakarta') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> </button></a>
                    <a href="{{ url('/karyakarta/' . $karyakarta->id . '/edit') }}" title="Edit Karyakartum"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>
                    {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['karyakarta', $karyakarta->id],
                    'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-sm',
                    'title' => 'Delete Karyakartum',
                    'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                    {!! Form::close() !!}
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <th>ID</th><td>{{ $karyakarta->id }}</td>
                                    <th> Name </th><td> {{ $karyakarta->name }} </td>
                                </tr>
                                <tr><th> Education </th><td> {{ $karyakarta->education }} </td>
                                    <th> Designation </th><td> {{ $karyakarta->designation }} </td>
                                </tr>
                                <tr>
                                    <th> Phone </th><td> {{ $karyakarta->phone }} </td>
                                    <th> Address </th><td> {{ $karyakarta->address }} </td>
                                </tr>
                                <tr>
                                    <th> Email </th><td> {{ $karyakarta->email }} </td>
                                    <th> Status </th><td> {{ $karyakarta->status }} </td>
                                </tr>
                                <tr>

                                    <th> Description </th><td> {{ $karyakarta->description }} </td>
                                    <th> Image </th><td><?php
                                        if (!empty($karyakarta->image) && @getimagesize($karyakarta->image)) {
                                            ?>
                                            <img src="{{ $karyakarta->image }}" style="width: 100px;height: auto;"/>
                                            <?php
                                        }
                                        ?> </td>
                                </tr>
                            </tbody>
                            <tr>


                                <th> Created At </th><td> {{ date('m/d/Y',strtotime($karyakarta->created_at)) }} </td>
                                <td colspan="2"> 

                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
