@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="card-header"> <h4 style=""><span>Assembly Assignment {{ $assignment->id }}</span></h4></div> 

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">

                    <a href="{{ url('/assignment') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> </button></a>
                    <a href="{{ url('/assignment/' . $assignment->id . '/edit') }}" title="Edit Assignment"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>
                    {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['assignment', $assignment->id],
                    'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-sm',
                    'title' => 'Delete Assignment',
                    'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                    {!! Form::close() !!}
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <th>ID</th><td>{{ $assignment->id }}</td>
                                </tr>
                                <tr><th> Pdf </th><td> <a href="{{ $assignment->pdf }}" target="_blank">{{ $assignment->display_name }}</a> </td></tr><tr><th> Level </th><td> {{ $assignment->levels->name ?? '' }} </td></tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
