@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="card-header"> <h4 style=""><span> Create Assembly Assignment </span></h4></div>

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
<!--                    <a href="{{ url('/assignment') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <br />
                    <br />-->

                    @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    @endif

                    {!! Form::open(['url' => '/assignment', 'class' => 'form-horizontal', 'files' => true]) !!}

                    @include ('admin.assignment.form', ['formMode' => 'create'])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
