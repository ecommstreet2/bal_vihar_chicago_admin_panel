
@include ('admin._user_level', ['formMode' => $formMode,'teacher_id'=>isset($assignment->teacher_id) ?$assignment->teacher_id : null,'level'=>isset($assignment->level) ?$assignment->level : null])
<div class="col-md-12 {{ $errors->has('pdf') ? 'has-error' : ''}}">
    {!! Form::label('pdf', 'Pdf', ['class' => 'control-label']) !!}
    {!! Form::file('pdf', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('pdf', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-6 {{ $errors->has('display_name') ? 'has-error' : ''}}">
    {!! Form::label('display_name', 'Pdf Display Name', ['class' => 'control-label']) !!}
    {!! Form::text('display_name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('display_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-12" style="margin-top: 10px;">
    <a href="{{ url('/assignment') }}" title="Back"><span class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</span></a>

    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
