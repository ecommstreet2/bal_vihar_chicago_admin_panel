<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-lightbox/0.7.0/bootstrap-lightbox.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-lightbox/0.7.0/bootstrap-lightbox.min.js"></script>
<style type="text/css">
    .lightbox{
        z-index: 1041;
    }
    .small-img{
        width: 100px;height: 100px;
    }
</style>

<div class="col-md-6 {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-6 {{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('image', 'Image', ['class' => 'control-label']) !!}
    {!! Form::file('image[]', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    {!! Form::hidden('gallery_id', $gallery_id,  ['id'=>'gallery_id','class' => 'form-control']) !!}
</div>

<div class="col-md-12" style="margin-top: 10px;">
    <a href="{{ url('/photo-gallery') }}" title="Back"><span class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</span></a>

    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
<div class="col-md-12" style="margin-top: 50px;">

    <?php
    foreach ($photogallies as $key => $photogallery) {
        ?>

        <a data-toggle="lightbox" href="#demoLightbox<?php echo $key; ?>" style="display: block;float: left;margin: 2px;">
            <img src="{{  asset($photogallery->thumbnail) }}" class="big-img">
        </a>
        <div id="demoLightbox<?php echo $key; ?>" class="lightbox fade"  tabindex="-1" role="dialog" aria-hidden="true">
            <div class='lightbox-dialog'>
                <div class='lightbox-content'>
                    <img src="{{  asset($photogallery->image) }}">
                    <div class='lightbox-caption'>
                        {{  $photogallery->name}}
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
    <div class="pagination-wrapper"> {!! $photogallies->appends(['id' =>$gallery_id,'search' => Request::get('search')])->render() !!} </div>

</div>