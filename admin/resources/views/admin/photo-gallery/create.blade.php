@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="card-header"> <h4 style=""><span>Photo Gallery</span></h4></div>

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
<!--                        <a href="{{ url('/photo-gallery') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <br />
                    <br />-->

<!--                    @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    @endif-->

                    {!! Form::open(['url' => '/photo-gallery', 'class' => 'form-horizontal', 'files' => true]) !!}

                    @include ('admin.photo-gallery.form', ['formMode' => 'create','photogallies'=>$photogallies])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
