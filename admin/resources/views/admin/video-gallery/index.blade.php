@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="card-header"> <h4 style=""><span>Video Gallery</span></h4></div>
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                    <!--                    <a href="{{ url('/photo-gallery/create') }}" class="btn btn-success btn-sm pull-left" title="Add New PhotoGallery">
                                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                                        </a>-->
                    {!! Form::open(['url' => '/video-galleries/create', 'class' => 'form-horizontal', 'files' => true]) !!}

                    <div class="col-md-4 {{ $errors->has('name') ? 'has-error' : ''}}">
                        {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                        {!! Form::text('name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                    </div>




                    <div class="col-md-4" style="margin-top: 26px;">
                        {!! Form::submit( 'Create', ['class' => 'btn btn-primary']) !!}
                    </div>
                    <div style="clear: both;margin-bottom: 50px;"></div>

                    {!! Form::close() !!}
                    {!! Form::open(['method' => 'GET', 'url' => '/video-gallery', 'class' => 'form-inline my-2 my-lg-0 float-right pull-right', 'role' => 'search'])  !!}

                    <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                    <span class="input-group-append">
                        <button class="btn btn-secondary" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>

                    {!! Form::close() !!}

                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        @foreach($videogallery as $item)

                                        <div class="col-md-2" style="margin: 5px;;font-size: 15px;padding: 5px;background-color: #e8f0fe;
                                             color: #1967d2;border: 1px solid #dadce0;border-radius: 6px;text-align: center;transition: box-shadow 200ms cubic-bezier(0.4,0.0,0.2,1);" ondblclick="showDirectory('{{$item->id}}')">
                                            <i class="fa fa-folder" style="margin-top: 4px;    margin-right: 5px;"> </i>{{$item->name}}</div>


                                        @endforeach
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $videogallery->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script>
                function showDirectory(id){
                window.location.href = "{{ url('/video-gallery/create?id=') }}" + id;
                }
    </script>
</div>
@endsection
