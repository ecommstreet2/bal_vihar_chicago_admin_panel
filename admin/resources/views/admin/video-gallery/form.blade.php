<div class="col-md-6 {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-6 {{ $errors->has('video') ? 'has-error' : ''}}">
    {!! Form::label('video', 'Video', ['class' => 'control-label']) !!}
    {!! Form::file('video[]', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('video', '<p class="help-block">:message</p>') !!}
    {!! Form::hidden('gallery_id', $gallery_id,  ['id'=>'gallery_id','class' => 'form-control']) !!}
</div>

<div class="col-md-12" style="margin-top: 10px;">
    <a href="{{ url('/photo-gallery') }}" title="Back"><span class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</span></a>

    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
<div class="col-md-12" style="margin-top: 50px;">

    <?php
    foreach ($videogallies as $key => $videogallery) {
        ?>
        <video width="320" height="240" controls>
            <source src="{{  asset($videogallery->video) }}" type="{{  $videogallery->mime_type }}">

        </video>


        <?php
    }
    ?>
    <div class="pagination-wrapper"> {!! $videogallies->appends(['id' =>$gallery_id,'search' => Request::get('search')])->render() !!} </div>

</div>