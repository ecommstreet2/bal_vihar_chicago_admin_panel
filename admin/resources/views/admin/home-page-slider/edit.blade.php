@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <div class="card-header"> <h4 style=""><span> Edit Slider  {{ $homePageSlider->post_title }} </span></h4></div>
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
<!--                    <a href="{{ url('/home-page-slider') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <br />
                    <br />-->

                    @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    @endif

                    {!! Form::model($homePageSlider, [
                    'method' => 'PATCH',
                    'url' => ['/home-page-slider', $homePageSlider->id],
                    'class' => 'form-horizontal',
                    'files' => true
                    ]) !!}

                    @include ('admin.home-page-slider.form', ['formMode' => 'edit'])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
