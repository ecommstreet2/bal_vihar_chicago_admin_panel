@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="card-header"> <h4 style=""><span> Home Page Slider </span></h4></div>

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                    <a href="{{ url('/home-page-slider/create') }}" class="btn btn-success btn-sm pull-left" title="Add New Slider">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>

                    {!! Form::open(['method' => 'GET', 'url' => '/home-page-slider', 'class' => 'form-inline my-2 my-lg-0 float-right pull-right', 'role' => 'search'])  !!}

                    <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                    <span class="input-group-append">
                        <button class="btn btn-secondary" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>

                    {!! Form::close() !!}

                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Date</th>
<!--                                    <th>Image</th>-->
<!--                                    <th>Status</th>-->
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($homePageSliders as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->post_title }}</td>
                                    <td>{{ date('m/d/Y',strtotime($item->post_date)) }}</td>
<!--                                    <td> <?php
//                                        if (!empty($item->slider_image) && @getimagesize($item->slider_image)) {
//                                            ?>
                                            <img src="{{ $item->slider_image }}" style="width: 100px;height: auto;"/>
                                            //<?php
//                                        }
                                        ?></td>-->
<!--                                    <td> {{ ($item->post_status==1)?'Active':'Inactive' }}</td>-->
<!--                                         <td> {{ $item->post_status }}</td>-->
                                    <td>
                                        <a href="{{ url('/home-page-slider/' . $item->id) }}" title="View Slider"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> </button></a>
                                        <a href="{{ url('/home-page-slider/' . $item->id . '/edit') }}" title="Edit Slider"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>
                                        {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => ['/home-page-slider', $item->id],
                                        'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-sm',
                                        'title' => 'Delete Slider',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                        )) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $homePageSliders->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
