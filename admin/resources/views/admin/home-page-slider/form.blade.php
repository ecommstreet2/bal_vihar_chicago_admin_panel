<div class="col-md-6 {{ $errors->has('post_title') ? 'has-error' : ''}}">
    {!! Form::label('post_title', 'Title', ['class' => 'control-label']) !!}
    {!! Form::text('post_title', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('post_title', '<p class="help-block">:message</p>') !!}
</div>


<div class="col-md-6 {{ $errors->has('post_date') ? 'has-error' : ''}}">
    {!! Form::label('post_date', 'Date', ['class' => 'control-label']) !!}
    {!! Form::text('post_date', !empty($homePageSlider->post_date)?date('m/d/Y', strtotime($homePageSlider->post_date)):null, ('' == 'required') ? ['class' => 'form-control date', 'required' => 'required'] : ['class' => 'form-control date']) !!}
    {!! $errors->first('post_date', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-6{{ $errors->has('slider_image') ? 'has-error' : ''}}">
    {!! Form::label('slider_image', 'Slider Image', ['class' => 'control-label']) !!}
    {!! Form::file('slider_image', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('slider_image', '<p class="help-block">:message</p>') !!}
</div>
<!--<div class="col-md-6   {{ $errors->has('post_status') ? 'has-error' : ''}}">
    {!! Form::label('post_status', 'Status', ['class' => 'control-label']) !!}
    {!! Form::select('post_status', ['1'=>'Active','0'=>'Inactive'], isset($homePageSlider->post_status) ?$homePageSlider->post_status : null, ['class' => 'form-control']) !!}

    {!! $errors->first('post_status', '<p class="help-block">:message</p>') !!}
</div>-->
<div class="col-md-6 {{ $errors->has('destination_link') ? 'has-error' : ''}}">
    {!! Form::label('destination_link', 'Destination Link', ['class' => 'control-label']) !!}
    {!! Form::text('destination_link', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('destination_link', '<p class="help-block">:message</p>') !!}
</div>



    <?php
    if (!empty($homePageSlider->guid) && @getimagesize($homePageSlider->guid)) {
        ?><div class="col-md-6">
        <img src="{{ $homePageSlider->guid }}" style="width: 100px;height: auto;"/>
        </div>
        <?php
    }
    ?>

<div class="col-md-6 {{ $errors->has('post_content') ? 'has-error' : ''}}">
    {!! Form::label('post_content', ' Description', ['class' => 'control-label']) !!}
    {!! Form::textarea('post_content', null, ('' == 'required') ? ['class' => 'form-control ckeditor', 'required' => 'required'] : ['class' => 'form-control ckeditor']) !!}
    {!! $errors->first('post_content', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-6 {{ $errors->has('link_title') ? 'has-error' : ''}}">
    {!! Form::label('link_title', 'Link Title', ['class' => 'control-label']) !!}
    {!! Form::text('link_title', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('link_title', '<p class="help-block">:message</p>') !!}
</div>


<div class="col-md-12" style="margin-top: 10px;">
    <a href="{{ url('/home-page-slider') }}" title="Back"><span class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</span></a>

    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>

<script type="text/javascript">
    $('.date').datepicker({
        format: 'mm/dd/yyyy'
    });
</script> 