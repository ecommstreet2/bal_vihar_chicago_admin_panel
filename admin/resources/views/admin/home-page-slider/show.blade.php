@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="card-header"> <h4 style=""><span> Slider {{ $homePageSlider->post_title }}</span></h4></div>

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">

                    <a href="{{ url('/home-page-slider') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i></button></a>
                    <a href="{{ url('/home-page-slider/' . $homePageSlider->id . '/edit') }}" title="Edit Slider"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>
                    {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['home-page-slider', $homePageSlider->id],
                    'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-sm',
                    'title' => 'Delete Slider',
                    'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                    {!! Form::close() !!}
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <th>ID</th><td>{{ $homePageSlider->id }}</td>
                                    <th>Title </th><td> {{ $homePageSlider->post_title }} </td>
                                </tr>
                                <tr>
                                    <th>Date</th><td> {{ date('m/d/Y',strtotime($homePageSlider->post_date)) }}</td>
                                    <th> Description </th><td>  {!! $homePageSlider->post_content !!}</td>
                                </tr>
                                <tr>
                                    <th> Destination Link </th>
                                    <td> 
                                       {!! $homePageSlider->destination_link !!}
                                    </td>

                                    <th> Link Title </th>
                            <td>   {!! $homePageSlider->link_title !!} </td>
                                    
                                </tr>
                                <tr>
                                    <th> Image </th>
                                    <td> 
                                        <?php
                                        if (!empty($homePageSlider->slider_image) && @getimagesize($homePageSlider->slider_image)) {
                                            ?>
                                            <img src="{{ $homePageSlider->slider_image }}" style="width: 100px;height: auto;"/>
                                            <?php
                                        }
                                        ?>
                                    </td>

                                    <th>  </th>

                                    <td> </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
