<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>  

<div class="col-md-6 {{ $errors->has('event_name') ? 'has-error' : ''}}">
    {!! Form::label('event_name', 'Event Name', ['class' => 'control-label']) !!}
    {!! Form::text('event_name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('event_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-6 {{ $errors->has('event_location') ? 'has-error' : ''}}">
    {!! Form::label('event_location', 'Location', ['class' => 'control-label']) !!}
    {!! Form::text('event_location', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('event_location', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-3 {{ $errors->has('event_start_date') ? 'has-error' : ''}}">
    {!! Form::label('event_start_date', 'Start Date', ['class' => 'control-label']) !!}
    {!! Form::text('event_start_date',  isset($event->event_start_date) ?date('m/d/Y',strtotime($event->event_start_date)) : null, ('' == 'required') ? ['class' => 'form-control date', 'required' => 'required'] : ['class' => 'form-control date']) !!}
    {!! $errors->first('event_start_date', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3 {{ $errors->has('event_start_time') ? 'has-error' : ''}}">
    {!! Form::label('event_start_time', 'Start Time', ['class' => 'control-label']) !!}
    {!! Form::text('event_start_time', null, ('' == 'required') ? ['class' => 'form-control time', 'required' => 'required'] : ['class' => 'form-control time']) !!}
    {!! $errors->first('event_start_time', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3 {{ $errors->has('event_end_date') ? 'has-error' : ''}}">
    {!! Form::label('event_end_date', 'End Date', ['class' => 'control-label']) !!}
    {!! Form::text('event_end_date',  isset($event->event_end_date) ?date('m/d/Y',strtotime($event->event_end_date)) : null, ('' == 'required') ? ['class' => 'form-control date', 'required' => 'required'] : ['class' => 'form-control date']) !!}
    {!! $errors->first('event_end_date', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3 {{ $errors->has('event_end_time') ? 'has-error' : ''}}">
    {!! Form::label('event_end_time', 'End Time', ['class' => 'control-label']) !!}
    {!! Form::text('event_end_time', null, ('' == 'required') ? ['class' => 'form-control time', 'required' => 'required'] : ['class' => 'form-control time']) !!}
    {!! $errors->first('event_end_time', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-6{{ $errors->has('event_image') ? 'has-error' : ''}}">
    {!! Form::label('event_image', 'Image', ['class' => 'control-label']) !!}
    {!! Form::file('event_image', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('event_image', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-6 {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Status', ['class' => 'control-label']) !!}
    {!! Form::select('status', ['1'=>'Active','0'=>'Inactive'], isset($event->status) ?$event->status : null, ['class' => 'form-control']) !!}
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>

<?php
if (!empty($event->event_image) && @getimagesize($event->event_image)) {
    ?>
    <div class="col-md-6">
        <img src="{{ $event->event_image }}" style="width: 100px;height: auto;"/>
    </div>
    <?php
}
?>

<div class="col-md-6 {{ $errors->has('event_description') ? 'has-error' : ''}}">
    {!! Form::label('event_description', 'Event Description', ['class' => 'control-label']) !!}
    {!! Form::textarea('event_description', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('event_description', '<p class="help-block">:message</p>') !!}
</div>



<div class="col-md-12" style="margin-top: 10px">
    <a href="{{ url('/event') }}" title="Back"><span class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</span></a>

    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
<script type="text/javascript">
$('.date').datepicker({
    format: 'mm/dd/yyyy'
});
$('.time').datetimepicker({
    format: 'HH:mm:ss'
});
</script> 