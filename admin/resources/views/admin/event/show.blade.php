@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="card-header"> <h4 style=""><span>Event {{ $event->id }}</span></h4></div>

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">

                    <a href="{{ url('/event') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> </button></a>
                    <a href="{{ url('/event/' . $event->id . '/edit') }}" title="Edit Event"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>
                    {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['event', $event->id],
                    'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-sm',
                    'title' => 'Delete Event',
                    'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                    {!! Form::close() !!}
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <th>ID</th><td>{{ $event->id }}</td>
                                </tr>
                                <tr><th> Event Name </th><td> {{ $event->event_name }} </td></tr>
                                <tr><th> Event Description </th><td> {{ $event->event_description }} </td></tr>
                                <tr><th> Event Start Date </th><td> {{ date('m/d/Y',strtotime($event->event_start_date)) }}  {{$event->event_start_time}} </td></tr>
                                <tr><th> Event End Time </th><td> {{ date('m/d/Y',strtotime($event->event_end_date)) }}  {{$event->event_end_time}} </td></tr>
                                <tr><th> Status </th><td> {{ ($event->status==1)?'Active':"Inactive" }} </td></tr>
                                <tr><th> Created At </th><td>{{ date('m/d/Y H:i:s',strtotime($event->created_at)) }}  </td></tr>
                                <tr><th> Updated At </th><td>{{ date('m/d/Y H:i:s',strtotime($event->updated_at)) }}  </td></tr>
                                <tr><th> Image </th><td>
                                        <?php
                                     
                                        if (!empty($event->event_image) && @getimagesize($event->event_image)) {
                                            ?>

                                            <img src="{{ $event->event_image }}" style="width: 100px;height: auto;"/>

                                            <?php
                                        }
                                        ?>
                                    </td></tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
