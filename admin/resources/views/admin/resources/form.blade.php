<div class="col-md-12 {{ $errors->has('weblink_url') ? 'has-error' : ''}}">
    {!! Form::label('weblink_url', 'Url', ['class' => 'control-label']) !!}
    {!! Form::text('weblink_url', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('weblink_url', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-12 {{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Title', ['class' => 'control-label']) !!}
    {!! Form::text('title', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>

@include ('admin._user_level', ['formMode' => $formMode,'teacher_id'=>isset($resource->teacher_id) ?$resource->teacher_id : null,'level'=>isset($resource->level) ?$resource->level : null])
<div class="col-md-12" style="margin-top: 10px;">
    <a href="{{ url('/resources') }}" title="Back"><span class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</span></a>

    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
