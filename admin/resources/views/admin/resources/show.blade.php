@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="card-header"> <h4 style=""><span> Teaching Resources Link {{ $resource->id }} </span></h4></div>

        <div class="col-md-12">
            <div class="card">
                
                <div class="card-body">

                    <a href="{{ url('/resources') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> </button></a>
                    <a href="{{ url('/resources/' . $resource->id . '/edit') }}" title="Edit Resource"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                    {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['resources', $resource->id],
                    'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-sm',
                    'title' => 'Delete Resource',
                    'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                    {!! Form::close() !!}
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <th>ID</th><td>{{ $resource->id }}</td>
                                </tr>
                                <tr><th> Weblink Url </th><td> {{ $resource->weblink_url }} </td></tr>
                                   <tr><th> Title </th><td> {{ $resource->title }} </td></tr>
                                <tr><th> Level </th><td> {{ $resource->levels->name ?? '' }} </td></tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
