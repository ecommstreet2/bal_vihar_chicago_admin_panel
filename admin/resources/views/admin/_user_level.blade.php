<?php
if (Auth::user()->isAdmin()) {
    ?>
    <div class="col-md-6 {{ $errors->has('level') ? 'has-error' : ''}}">
        {!! Form::label('teacher_id', 'Teacher', ['class' => 'control-label']) !!}
        {!! Form::select('teacher_id', \App\models\Teacher::where('user_type','TEACHER')->pluck('display_name', 'id'),$teacher_id, ['class' => 'form-control','placeholder' => 'Select Teacher']) !!}

        {!! $errors->first('teacher_id', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-md-6 {{ $errors->has('level') ? 'has-error' : ''}}">
        {!! Form::label('level', 'Level', ['class' => 'control-label']) !!}
        <?php
        if ($formMode == 'create') {
            $levelArr = [];
        } else {
            $levelArr = \DB::table("levels")
                    ->join('wp_users_level', 'wp_users_level.level_id', '=', 'levels.id')
                    ->where("wp_users_level.user_id", $teacher_id)
                    ->pluck("levels.name", "levels.id");
        }
        ?>
        {!! Form::select('level',$levelArr,$level, ['class' => 'form-control','placeholder' => 'Select Level']) !!}

        {!! $errors->first('level', '<p class="help-block">:message</p>') !!}
    </div>
    <?php
} else {
    ?>  
    <div class="col-md-6 {{ $errors->has('level') ? 'has-error' : ''}}">
        {!! Form::label('level', 'Level', ['class' => 'control-label']) !!}
        <?php
        $teacherLevelArr = \DB::table("levels")
                ->join('wp_users_level', 'wp_users_level.level_id', '=', 'levels.id')
                ->where("wp_users_level.user_id", Auth::user()->ID)
                ->pluck("levels.name", "levels.id");
        ?>
        {!! Form::select('level', $teacherLevelArr,$level, ['class' => 'form-control']) !!}

        {!! $errors->first('level', '<p class="help-block">:message</p>') !!}
    </div>
    <?php
}
?>



<script type="text/javascript">
    $(document).ready(function () {
        $('select[name="teacher_id"]').on('change', function () {
            var teacherID = $(this).val();
            if (teacherID) {
                $.ajax({
                    url: "{{ url('teacherLevel/') }}" + '/' + teacherID,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {


                        $('select[name="level"]').empty();
                        $.each(data, function (key, value) {
                            $('select[name="level"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                });
            } else {
                $('select[name="level"]').empty();
            }
        });
    });
</script>