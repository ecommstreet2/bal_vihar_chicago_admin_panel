@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="card-header"> <h4 style=""><span> News {{ $news->post_title }}</span></h4></div>

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">

                    <a href="{{ url('/news') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i></button></a>
                    <a href="{{ url('/news/' . $news->ID . '/edit') }}" title="Edit News"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>
                    {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['news', $news->ID],
                    'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-sm',
                    'title' => 'Delete News',
                    'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                    {!! Form::close() !!}
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <th>ID</th><td>{{ $news->ID }}</td>
                                    <th>Title </th><td> {{ $news->post_title }} </td>
                                </tr>
                                <tr>
                                    <th>Date</th><td> {{ date('m/d/Y',strtotime($news->post_date)) }}</td>
                                    <th> Description </th><td>  {!! $news->post_content !!}</td>
                                </tr>
                                <tr>
                                    <th> Image </th>
                                    <td  style="width: 100px;" > 
                                        <?php
                                        if (!empty($news->guid) && @getimagesize( $news->guid)) {
                                            ?>
                                            <img src="{{ $news->guid }}" style="width: 100px;height: auto;"/>
                                            <?php
                                        }
                                        ?>
                                    </td>

                                    <th> Status </th>
<!--                                    <td> {{ ($news->post_status==1)?'Active':'Inactive' }} </td>-->
                                    <td> {{ $news->post_status }}</td>
                                </tr>


                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
