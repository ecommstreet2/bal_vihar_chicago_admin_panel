<div class="col-md-6{{ $errors->has('post_title') ? 'has-error' : ''}}">
    {!! Form::label('post_title', 'News Title', ['class' => 'control-label']) !!}
    {!! Form::text('post_title', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('post_title', '<p class="help-block">:message</p>') !!}
</div>


<div class="col-md-6{{ $errors->has('post_date') ? 'has-error' : ''}}">
    {!! Form::label('post_date', 'News Date', ['class' => 'control-label']) !!}
    {!! Form::text('post_date', !empty($news->post_date)?date('m/d/Y', strtotime($news->post_date)):null, ('' == 'required') ? ['class' => 'form-control date', 'required' => 'required'] : ['class' => 'form-control date']) !!}
    {!! $errors->first('post_date', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-6{{ $errors->has('news_image') ? 'has-error' : ''}}">
    {!! Form::label('news_image', 'News Image', ['class' => 'control-label']) !!}
    {!! Form::file('news_image', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('news_image', '<p class="help-block">:message</p>') !!}
</div>
<!--<div class="col-md-6   {{ $errors->has('post_status') ? 'has-error' : ''}}">
    {!! Form::label('post_status', 'Status', ['class' => 'control-label']) !!}
    {!! Form::select('post_status', ['1'=>'Active','0'=>'Inactive'], isset($news->post_status) ?$news->post_status : null, ['class' => 'form-control']) !!}

    {!! $errors->first('post_status', '<p class="help-block">:message</p>') !!}
</div>-->
<div class="col-md-6">
    <?php
    if (!empty($news->guid) && @getimagesize( $news->guid)) {
        ?>
        <img src="{{ $news->guid }}" style="width: 100px;height: auto;"/>
        <?php
    }
    ?>
</div>
<div class="col-md-7 {{ $errors->has('post_content') ? 'has-error' : ''}}">
    {!! Form::label('post_content', 'News Dscription', ['class' => 'control-label']) !!}
    {!! Form::textarea('post_content', null, ('' == 'required') ? ['class' => 'form-control ckeditor', 'required' => 'required'] : ['class' => 'form-control ckeditor']) !!}
    {!! $errors->first('post_content', '<p class="help-block">:message</p>') !!}
</div>



<div class="col-md-12" style="margin-top: 10px;">
    <a href="{{ url('/news') }}" title="Back"><span class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</span></a>

    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>

<script type="text/javascript">
    $('.date').datepicker({
        format: 'mm/dd/yyyy'
    });
</script> 