@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="card-header"> <h4 style=""><span> News </span></h4></div>

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                    <a href="{{ url('/news/create') }}" class="btn btn-success btn-sm pull-left" title="Add New News">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>

                    {!! Form::open(['method' => 'GET', 'url' => '/news', 'class' => 'form-inline my-2 my-lg-0 float-right pull-right', 'role' => 'search'])  !!}

                    <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                    <span class="input-group-append">
                        <button class="btn btn-secondary" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>

                    {!! Form::close() !!}

                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Date</th>
<!--                                    <th>Image</th>-->
                                    <th>Status</th><th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($news as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->post_title }}</td>
                                    <td>{{ date('m/d/Y',strtotime($item->post_date)) }}</td>
<!--                                    <td> <?php
//                                        if (!empty($item->news_image) && @getimagesize($item->news_image)) {
//                                            ?>
                                            <img src="{{ $item->news_image }}" style="width: 100px;height: auto;"/>
                                            //<?php
//                                        }
                                        ?></td>-->
<!--                                    <td> {{ ($item->post_status==1)?'Active':'Inactive' }}</td>-->
                                         <td> {{ $item->post_status }}</td>
                                    <td>
                                        <a href="{{ url('/news/' . $item->ID) }}" title="View News"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> </button></a>
                                        <a href="{{ url('/news/' . $item->ID . '/edit') }}" title="Edit News"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>
                                        {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => ['/news', $item->ID],
                                        'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-sm',
                                        'title' => 'Delete News',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                        )) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $news->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
