<?php
//dd($levels);
?>

<div class="col-md-12"><h5 class="well">USER INFORMATION</h5></div>
<div class="col-md-12">
    <!--    <div class="col-md-4">
            <div class="{{ $errors->has('user_login') ? 'has-error' : ''}}">
                <label for="user_login" class="control-label">{{ 'Username' }}</label>
                {!! Form::text('user_login', isset($user->user_login) ? $user->user_login : null, ['class' => 'form-control']) !!}
                {!! $errors->first('user_login', '<p class="help-block">:message</p>') !!}
            </div>
    
        </div>-->
    <div class="col-md-4">
        <div class=" {{ $errors->has('user_email') ? 'has-error' : ''}}">
            <label for="user_email" class="control-label">{{ 'Email' }}</label>
            <?php
            if ($formMode == 'create') {
                ?>
                {!! Form::text('user_email', isset($user->user_email) ? $user->user_email : null, ['class' => 'form-control']) !!} 
                <?php
            } else {
                ?>
                {!! Form::text('user_email', isset($user->user_email) ? $user->user_email : null, ['class' => 'form-control','readonly']) !!} 
                <?php
            }
            ?>

            {!! $errors->first('user_email', '<p class="help-block">:message</p>') !!}
        </div>

    </div>
    <?php
    if ($formMode == 'create') {
        ?>
        <div class="col-md-4">
            <div class=" {{ $errors->has('user_pass') ? 'has-error' : ''}}">
                <label for="user_pass" class="control-label">{{ 'Password' }}</label>
                {!! Form::text('user_pass', isset($user->user_pass) ? $user->user_pass : null, ['class' => 'form-control']) !!}
                {!! $errors->first('user_pass', '<p class="help-block">:message</p>') !!}
            </div>

        </div>
        <?php
    }
    ?>
    <div class="col-md-4">
        <div class=" {{ $errors->has('user_nicename') ? 'has-error' : ''}}">
            <label for="user_nicename" class="control-label">{{ 'Nick Name' }}</label>
            {!! Form::text('user_nicename', isset($user->user_nicename) ?$user->user_nicename : null, ['class' => 'form-control']) !!}
            {!! $errors->first('user_nicename', '<p class="help-block">:message</p>') !!}
        </div>

    </div>
    <div class="col-md-4">
        <div class=" {{ $errors->has('display_name') ? 'has-error' : ''}}">
            <label for="display_name" class="control-label">{{ 'Display Name' }}</label>
            {!! Form::text('display_name', isset($user->display_name) ?$user->display_name : null, ['class' => 'form-control']) !!}
            {!! $errors->first('display_name', '<p class="help-block">:message</p>') !!}
        </div>

    </div>
    <div class="col-md-4">
        <!--        <div class=" {{ $errors->has('user_url') ? 'has-error' : ''}}">
                    <label for="user_url" class="control-label">{{ 'User Url' }}</label>
                    {!! Form::text('user_url', isset($parent->user_url) ? $parent->user_url : null, ['class' => 'form-control']) !!}
                    {!! $errors->first('user_url', '<p class="help-block">:message</p>') !!}
                </div>-->

    </div>
</div>


<div class="col-md-12"><h5 class="well">LEVEL INFORMATION</h5></div>
<div class="col-md-12">

    <div class="col-md-4">
        <div class=" {{ $errors->has('name') ? 'has-error' : ''}}">
            <label for="name" class="control-label">{{ 'Levels' }}</label>

            {!! Form::select('level_id[]', \App\models\Level::pluck('name', 'id'), $levels, ['class' => 'form-control','multiple'=>true]) !!}
            {!! $errors->first('level_id[]', '<p class="help-block">:message</p>') !!}
        </div>

    </div>


</div>
<div class="col-md-12"><h5 class="well">PARENT INFORMATION</h5></div>

<div class="col-md-12">
    <div class="col-md-4">
        <div class="{{ $errors->has('mother_first_name') ? 'has-error' : ''}}">
            <label for="mother_first_name" class="control-label">{{ 'Mother First Name' }}</label>
            {!! Form::text('mother_first_name', isset($parent->mother_first_name) ? $parent->mother_first_name : null, ['class' => 'form-control']) !!}
            {!! $errors->first('mother_first_name', '<p class="help-block">:message</p>') !!}
        </div>

    </div>
    <div class="col-md-4">
        <div class=" {{ $errors->has('mother_last_name') ? 'has-error' : ''}}">
            <label for="mother_last_name" class="control-label">{{ 'Mother Last Name' }}</label>
            {!! Form::text('mother_last_name', isset($parent->mother_last_name) ? $parent->mother_last_name : null, ['class' => 'form-control']) !!}
            {!! $errors->first('mother_last_name', '<p class="help-block">:message</p>') !!}
        </div>

    </div>
    <div class="col-md-4">
        <div class=" {{ $errors->has('fother_first_name') ? 'has-error' : ''}}">
            <label for="fother_first_name" class="control-label">{{ 'Father First Name' }}</label>
            {!! Form::text('fother_first_name', isset($parent->fother_first_name) ? $parent->fother_first_name : null, ['class' => 'form-control']) !!}
            {!! $errors->first('fother_first_name', '<p class="help-block">:message</p>') !!}
        </div>

    </div>
    <div class="col-md-4">
        {!! Form::hidden('parent_id', isset($parent->id) ? $parent->id : null, ['readonly']) !!}
        <div class=" {{ $errors->has('fother_last_name') ? 'has-error' : ''}}">
            <label for="fother_last_name" class="control-label">{{ 'Father Last Name' }}</label>
            {!! Form::text('fother_last_name', isset($parent->fother_last_name) ? $parent->fother_last_name : null, ['class' => 'form-control']) !!}
            {!! $errors->first('fother_last_name', '<p class="help-block">:message</p>') !!}
        </div>

    </div>
    <div class="col-md-4">
        <div class=" {{ $errors->has('home_phone_number') ? 'has-error' : ''}}">
            <label for="home_phone_number" class="control-label">{{ 'Home Phone Number' }}</label>
            {!! Form::text('home_phone_number', isset($parent->home_phone_number) ? $parent->home_phone_number : null, ['class' => 'form-control']) !!}
            {!! $errors->first('home_phone_number', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class=" {{ $errors->has('mother_mobile_number') ? 'has-error' : ''}}">
            <label for="mother_mobile_number" class="control-label">{{ 'Mother Mobile Number' }}</label>
            {!! Form::text('mother_mobile_number', isset($parent->mother_mobile_number) ? $parent->mother_mobile_number : null, ['class' => 'form-control']) !!}
            {!! $errors->first('mother_mobile_number', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class=" {{ $errors->has('father_mobile_number') ? 'has-error' : ''}}">
            <label for="father_mobile_number" class="control-label">{{ 'Father Mobile Number' }}</label>
            {!! Form::text('father_mobile_number', isset($parent->father_mobile_number) ? $parent->father_mobile_number : null, ['class' => 'form-control']) !!}
            {!! $errors->first('father_mobile_number', '<p class="help-block">:message</p>') !!}
        </div>

    </div>
    <div class="col-md-4">
        <div class=" {{ $errors->has('parent_email') ? 'has-error' : ''}}">
            <label for="parent_email" class="control-label">{{ 'Email' }}</label>
            {!! Form::text('parent_email', isset($parent->parent_email) ? $parent->parent_email : null, ['class' => 'form-control']) !!}
            {!! $errors->first('parent_email', '<p class="help-block">:message</p>') !!}
        </div>

    </div>
    <div class="col-md-4">
        <div class=" {{ $errors->has('address') ? 'has-error' : ''}}">
            <label for="address" class="control-label">{{ 'Address' }}</label>
            {!! Form::text('address', isset($parent->address) ? $parent->address : null, ['class' => 'form-control']) !!}
            {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class=" {{ $errors->has('city') ? 'has-error' : ''}}">
            <label for="city" class="control-label">{{ 'City' }}</label>
            {!! Form::text('city', isset($parent->city) ? $parent->city : null, ['class' => 'form-control']) !!}
            {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class=" {{ $errors->has('state') ? 'has-error' : ''}}">
            <label for="state" class="control-label">{{ 'State' }}</label>
            {!! Form::text('state', isset($parent->state) ? $parent->state : null, ['class' => 'form-control']) !!}
            {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
        </div>

    </div>
    <div class="col-md-4">
        <div class=" {{ $errors->has('zip') ? 'has-error' : ''}}">
            <label for="zip" class="control-label">{{ 'Zip' }}</label>
            {!! Form::text('zip', isset($parent->zip) ? $parent->zip : null, ['class' => 'form-control']) !!}
            {!! $errors->first('zip', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class=" {{ $errors->has('transaction_id') ? 'has-error' : ''}}">
            <label for="transaction_id" class="control-label">{{ 'Transaction Id' }}</label>
            {!! Form::text('transaction_id', isset($parent->transaction_id) ? $parent->transaction_id : null, ['class' => 'form-control','readonly' ]) !!}
            {!! $errors->first('transaction_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>


<div class="col-md-12"><h5 class="well">STUDENT INFORMATION</h5></div>
<?php
if (($student)) {

    foreach ($student as $key => $stdnt) {
        if ($stdnt) {
            ?>
            <div class="col-md-12">

                <div class="col-md-4">
                    <div class=" {{ $errors->has('first_name') ? 'has-error' : ''}}">
                        <label for="first_name" class="control-label">{{ 'First Name' }}</label>
                        {!! Form::hidden('student[id][]', isset($stdnt->id) ? $stdnt->id : null, ['readonly']) !!}
                        {!! Form::text('student[first_name][]', isset($stdnt->first_name) ? $stdnt->first_name : null, ['class' => 'form-control']) !!}
                        {!! $errors->first('student[first_name][]', '<p class="help-block">:message</p>') !!}
                    </div>

                </div>
                <div class="col-md-4">
                    <div class=" {{ $errors->has('last_name') ? 'has-error' : ''}}">
                        <label for="last_name" class="control-label">{{ 'Last Name' }}</label>
                        {!! Form::text('student[last_name][]', isset($stdnt->last_name) ? $stdnt->last_name : null, ['class' => 'form-control']) !!}
                        {!! $errors->first('student[last_name][]', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class=" {{ $errors->has('birth_date') ? 'has-error' : ''}}">
                        <label for="birth_date" class="control-label">{{ 'DOB' }}</label>
                        {!! Form::text('student[birth_date][]', isset($stdnt->birth_date) ? $stdnt->birth_date : null, ['class' => 'form-control' ]) !!}
                        {!! $errors->first('student[birth_date][]', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-4">
                    <div class=" {{ $errors->has('apply_level') ? 'has-error' : ''}}">
                        <label for="apply_level" class="control-label">{{ 'Applied Level' }}</label>
                        {!! Form::text('student[apply_level][]', isset($stdnt->apply_level) ? $stdnt->apply_level : null, ['class' => 'form-control']) !!}
                        {!! $errors->first('student[apply_level][]', '<p class="help-block">:message</p>') !!}
                    </div>

                </div>


            </div>
            <?php
        }
    }
}
?>
<div class="col-md-12"><h5 class="well">PHYSICIAN MEDICAL</h5></div>
<div class="col-md-12">
    <div class="col-md-4">
        <div class=" {{ $errors->has('name') ? 'has-error' : ''}}">
            <label for="name" class="control-label">{{ 'Name' }}</label>
            {!! Form::text('name', isset($studentMedical->name) ? $studentMedical->name : null, ['class' => 'form-control']) !!}
            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        </div>

    </div>
    <div class="col-md-4">
        <div class=" {{ $errors->has('email') ? 'has-error' : ''}}">
            <label for="email" class="control-label">{{ 'Email' }}</label>
            {!! Form::text('email', isset($studentMedical->email) ? $studentMedical->email : null, ['class' => 'form-control']) !!}
            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class=" {{ $errors->has('phone') ? 'has-error' : ''}}">
            <label for="phone" class="control-label">{{ 'Phone' }}</label>
            {!! Form::text('phone', isset($studentMedical->phone) ? $studentMedical->phone : null, ['class' => 'form-control' ]) !!}
            {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="col-md-4">
        <div class=" {{ $errors->has('sm_address') ? 'has-error' : ''}}">
            <label for="sm_address" class="control-label">{{ 'Address' }}</label>
            {!! Form::text('sm_address', isset($studentMedical->address) ? $studentMedical->address : null, ['class' => 'form-control']) !!}
            {!! $errors->first('sm_address', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<div class="col-md-12"><h5 class="well">IN CASE OF EMERGENCY CONTACT</h5></div>
<?php
if (($studentRelationContact)) {

    foreach ($studentRelationContact as $key => $relationContact) {
        if ($relationContact) {
            ?>
            <div class="col-md-12" style="margin-top: 7px;">  <label class="pull-left">CONTACT <?php echo ($key + 1); ?></label></div>
            <div class="col-md-12">

                <div class="col-md-4">
                    <div class=" {{ $errors->has('first_name') ? 'has-error' : ''}}">
                        <label for="first_name" class="control-label">{{ 'First Name' }}</label>
                        {!! Form::hidden('emg[id][]', isset($relationContact->id) ? $relationContact->id : null, ['readonly']) !!}
                        {!! Form::text('emg[first_name][]', isset($relationContact->first_name) ? $relationContact->first_name : null, ['class' => 'form-control']) !!}
                        {!! $errors->first('emg[first_name][]', '<p class="help-block">:message</p>') !!}
                    </div>

                </div>
                <div class="col-md-4">
                    <div class=" {{ $errors->has('last_name') ? 'has-error' : ''}}">
                        <label for="last_name" class="control-label">{{ 'Last Name' }}</label>
                        {!! Form::text('emg[last_name][]', isset($relationContact->last_name) ? $relationContact->last_name : null, ['class' => 'form-control']) !!}
                        {!! $errors->first('emg[last_name][]', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class=" {{ $errors->has('mobile') ? 'has-error' : ''}}">
                        <label for="mobile" class="control-label">{{ 'Mobile' }}</label>
                        {!! Form::text('emg[mobile][]', isset($relationContact->mobile) ? $relationContact->mobile : null, ['class' => 'form-control' ]) !!}
                        {!! $errors->first('emg[mobile][]', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class=" {{ $errors->has('relation') ? 'has-error' : ''}}">
                        <label for="relation" class="control-label">{{ 'Relation' }}</label>
                        {!! Form::text('emg[relation][]', isset($relationContact->relation) ? $relationContact->relation : null, ['class' => 'form-control' ]) !!}
                        {!! $errors->first('emg[relation][]', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
            <?php
        }
    }
}
?>
<div class="col-md-12" style="margin-top: 10px;">
 <a href="{{ url('/parent') }}" title="Back"><span class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</span></a>
                       
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">

</div>