@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
  <div class="card-header"> <h4 style=""><span>Parent  {{ $parent->fother_first_name }}</span></h4></div>

        <div class="col-md-12">
            <div class="card">
              
                <div class="card-body">

                    <a href="{{ url('/parent') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> </button></a>
                    <a href="{{ url('/parent/' . $parent->id . '/edit') }}" title="Edit Parent"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>

                    <form method="POST" action="{{ url('parent' . '/' . $parent->id) }}" accept-charset="UTF-8" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger btn-sm" title="Delete Parent" onclick="return confirm( & quot; Confirm delete? & quot; )"><i class="fa fa-trash-o" aria-hidden="true"></i> </button>
                    </form>
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td colspan="4"><h5 class="well">USER INFORMATION</h5></td>
                                </tr>
                                <tr>
                                    <th>ID</th><td>{{ $user->ID }}</td>
                                    <th>Email </th><td>{{ $user->user_email }}</td>

                                </tr>

                                <tr>
                                    <th>Display Name</th><td>{{ $user->display_name }}</td>
                                    <th>Nick Name </th><td>{{ $user->user_nicename }}</td>

                                </tr>


                                <tr>
                                    <th>Type</th><td>{{ $user->user_type }}</td>
                                    <th>Created At </th><td>{{ date('m/d/Y h:i:s A',strtotime($user->user_registered)) }}</td>

                                </tr>
                                <tr>
                                    <td colspan="4"><h5 class="well">LEVEL INFORMATION</h5></td>
                                </tr>


                                <?php
                                if (($parent->levels)) {
                                    foreach ($parent->levels as $key => $lvl) {
                                        if ($lvl) {
                                            ?>
                                            <tr>
                                                <th>ID</th><td>{{ $lvl->id }}</td>
                                                <th>Name </th><td>{{ $lvl->name }}</td>

                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                                ?>


                                <tr>
                                    <td colspan="4"><h5 class="well">PARENT INFORMATION</h5></td>
                                </tr>
                                <tr>
                                    <th>ID</th><td>{{ $parent->id }}</td>
                                    <th>Transaction Id</th><td>{{ $parent->transaction_id }}</td>

                                </tr>

                                <tr>
                                    <th> Father Name </th><td> {{ $parent->fother_first_name }}   {{ $parent->fother_last_name }} </td>
                                    <th> Mother Name </th><td> {{ $parent->mother_first_name }}   {{ $parent->mother_last_name }} </td>

                                </tr>

                                <tr>
                                    <th> Home Phone Number</th><td> {{ $parent->home_phone_number }}   </td>
                                    <th>Mother Phone Number </th><td> {{ $parent->mother_mobile_number }}   </td>

                                </tr>
                                <tr>

                                    <th>Father Phone Number </th><td> {{ $parent->father_mobile_number }}   </td>
                                    <th>Address </th><td> {{ $parent->address }}   </td>
                                </tr>
                                <tr>

                                    <th>City</th><td> {{ $parent->city }}   </td>
                                    <th>State </th><td> {{ $parent->state }}   </td>
                                </tr>
                                <tr>

                                    <th>Zip </th><td> {{ $parent->zip }}   </td>
                                    <th>Created At </th><td> {{ date('m/d/Y',strtotime($parent->created_at)) }}   </td>
                                </tr>
                                <tr>
                                    <td colspan="4"><h5 class="well">STUDENT INFORMATION</h5> </td>
                                </tr>
                                <?php
                                if (($parent->student)) {

                                    foreach ($parent->student as $key => $student) {
                                        if ($student) {
                                            ?>
                                            <tr>
                                                <th> Name </th><td> {{ $student->first_name ?? '' }}   {{ $student->last_name ?? '' }} </td>
                                                <th> DOB </th><td> {{ date('m/d/Y',strtotime($student->birth_date ?? '')) }}     </td>

                                            </tr>
                                            <tr>
                                                <th> Apply Level </th><td> {{ $student->apply_level }}  </td>
                                                <th> </th><td></td>

                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                                <tr>
                                    <td colspan="4"><h5 class="well">PHYSICIAN MEDICAL</h5> </td>
                                </tr>
                                <tr>
                                    <th> Name </th><td> {{ $parent->studentMedical->name ?? '' }}   </td>
                                    <th> Email </th><td> {{ $parent->studentMedical->email ?? '' }}    </td>

                                </tr>
                                <tr>
                                    <th> Phone </th><td> {{ $parent->studentMedical->phone }}  </td>
                                    <th> Address </th><td> {{ $parent->studentMedical->address }}  </td>

                                </tr>
                                <tr>
                                    <td colspan="4"><h5 class="well">IN CASE OF EMERGENCY CONTACT</h5> </td>
                                </tr>
                                <?php
                                if (($parent->studentRelationContact)) {

                                    foreach ($parent->studentRelationContact as $key => $studentRelationContact) {
                                        if ($studentRelationContact) {
                                            ?>
                                            <tr>
                                                <td colspan="4"><label>CONTACT <?php echo ($key + 1); ?></label> </td>
                                            </tr>
                                            <tr>
                                                <th> Name </th><td> {{ $studentRelationContact->first_name ?? '' }} {{ $studentRelationContact->last_name ?? '' }}   </td>
                                                <th> Mobile </th><td> {{ $studentRelationContact->mobile ?? '' }}    </td>

                                            </tr>
                                            <tr>
                                                <th> Relation </th><td> {{$studentRelationContact->relation }}  </td>
                                                <th>  </th><td>  </td>

                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
