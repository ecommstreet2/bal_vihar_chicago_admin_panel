@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="card-header"> <h4 style=""><span>Parents</span></h4></div>
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
<!--                    <a href="{{ url('/parent/create') }}" class="btn btn-success btn-sm  pull-left" title="Add New Parent">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>-->

                    <form method="GET" action="{{ url('/parent') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right pull-right" role="search">

                        <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                        <span class="input-group-append">
                            <button class="btn btn-secondary" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>

                    </form>

                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Transaction Id</th>
                                    <th>Mother Name</th>
                                    <th>Father Name</th>
                                    <th>Address</th>
                                    <th>Father Mobile Number</th>

                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($parent as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->transaction_id }}</td>
                                    <td>{{ $item->mother_first_name }} {{ $item->mother_last_name }}</td>
                                    <td>{{ $item->fother_first_name }} {{ $item->fother_last_name }}</td>
                                    <td>{{ $item->city }}, {{ $item->state }}</td>
                                    <td>{{ $item->father_mobile_number }}</td>
                                    <td>
                                        <?php
                                        if (isset($item->user_id) && !empty($item->user_id)) {
                                            ?>
                                            <a href="{{ url('/parent/' . $item->id . '/edit') }}" title="Edit Parent"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>
                                            <?php
                                        } else {
                                            ?>
                                            <a href="{{ url('/parent/create?id='. $item->id) }}" title="Generate Credential"><button class="btn btn-primary btn-sm"><i class="fa fa-key" aria-hidden="true"></i> </button></a>
                                            <?php
                                        }
                                        ?>

                                        <a href="{{ url('/parent/' . $item->id) }}" title="View Parent"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> </button></a>


                                        <form method="POST" action="{{ url('/parent' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Parent" onclick="return confirm( & quot; Confirm delete? & quot; )"><i class="fa fa-trash-o" aria-hidden="true"></i> </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $parent->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
