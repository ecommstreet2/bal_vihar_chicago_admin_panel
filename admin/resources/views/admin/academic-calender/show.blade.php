@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="card-header"> <h4 style=""><span>Academic Calender #{{ $academicCalender->id }}</span></h4></div>

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">

                    <a href="{{ url('/academic-calender') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> </button></a>
                    <a href="{{ url('/academic-calender/' . $academicCalender->id . '/edit') }}" title="Edit Syllabus Calender"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>
                    {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['latestupdates', $academicCalender->id],
                    'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-sm',
                    'title' => 'Delete Syllabus Calender',
                    'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                    {!! Form::close() !!}
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <th>ID</th><td>{{ $academicCalender->id }}</td>
                                </tr>
                                <tr>
                                    <th>Title</th><td>{{ $academicCalender->title }}</td>
                                </tr>
                                <tr><th> Date </th> <td>{{ date('m/d/Y',strtotime($academicCalender->calender_date)) }}</td></tr>
                                <tr><th> Description </th><td>{!! $academicCalender->description !!}  </td></tr>
                                 <tr><th> Level </th><td>{{ $academicCalender->levels->name ?? '' }}  </td></tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
