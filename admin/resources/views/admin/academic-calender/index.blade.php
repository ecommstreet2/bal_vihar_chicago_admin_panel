@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="card-header"> <h4 style=""><span>Academic Calender</span></h4></div>
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                    <a href="{{ url('/academic-calender/create') }}" class="btn btn-success btn-sm pull-left" title="Add New Syllabus Calender">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>

                    {!! Form::open(['method' => 'GET', 'url' => '/academic-calender', 'class' => 'form-inline my-2 my-lg-0 float-right pull-right', 'role' => 'search'])  !!}

                    <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                    <span class="input-group-append">
                        <button class="btn btn-secondary" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>

                    {!! Form::close() !!}

                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th>#</th><th>Event Name</th><th>Event Description</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($academicCalender as $item)

                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->post_title }}</td>
                            <td style="word-wrap: break-word;"> {{ $item->post_content }}</td>
                            <td>{{ date('m/d/Y',strtotime($item->post_date)) }}  {{date('H:i:s',strtotime($item->post_date))}}</td>
                            <td>{{ date('m/d/Y',strtotime($item->post_date)) }}  {{date('H:i:s',strtotime($item->post_date))}}</td>

                            <td>
                                <a href="{{ url('/academic-calender/' . $item->ID) }}" title="View Syllabus Calender"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> </button></a>
                                <a href="{{ url('/academic-calender/' . $item->ID . '/edit') }}" title="Edit Syllabus Calender"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>
                                {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/academic-calender', $item->ID],
                                'style' => 'display:inline'
                                ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ', array(
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-sm',
                                'title' => 'Delete Syllabus Calender',
                                'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                                {!! Form::close() !!}
                            </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $academicCalender->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
