<div class="col-md-12">

    <div class="col-md-6">
        <div class=" {{ $errors->has('user_email') ? 'has-error' : ''}}">
            <label for="user_email" class="control-label">{{ 'Email' }}</label>
            <?php
            if ($formMode == 'create') {
                ?>
                {!! Form::email('user_email', isset($teacher->user_email) ? $teacher->user_email : null, ['class' => 'form-control']) !!} 
                <?php
            } else {
                ?>
                {!! Form::email('user_email', isset($teacher->user_email) ? $teacher->user_email : null, ['class' => 'form-control','readonly']) !!} 
                <?php
            }
            ?>
<!--            {!! $errors->first('user_email', '<p class="help-block">:message</p>') !!}-->
        </div>

    </div>
    <?php
    if ($formMode == 'create') {
        ?>
        <div class="col-md-6">
            <div class=" {{ $errors->has('user_pass') ? 'has-error' : ''}}">
                <label for="user_pass" class="control-label">{{ 'Password' }}</label>
                {!! Form::password('user_pass',  ['class' => 'form-control']) !!}
    <!--                {!! $errors->first('user_pass', '<p class="help-block">:message</p>') !!}-->
            </div>

        </div>
        <?php
    }
    ?>
<!--    <div class="col-md-6">
        <div class=" {{ $errors->has('user_nicename') ? 'has-error' : ''}}">
            <label for="user_nicename" class="control-label">{{ 'Nick Name' }}</label>
            {!! Form::text('user_nicename', isset($teacher->user_nicename) ?$teacher->user_nicename : null, ['class' => 'form-control']) !!}
            {!! $errors->first('user_nicename', '<p class="help-block">:message</p>') !!}
        </div>

    </div>-->
    <div class="col-md-6">
        <div class=" {{ $errors->has('display_name') ? 'has-error' : ''}}">
            <label for="display_name" class="control-label">{{ 'Name' }}</label>
            {!! Form::text('display_name', isset($teacher->display_name) ?$teacher->display_name : null, ['class' => 'form-control']) !!}
<!--            {!! $errors->first('display_name', '<p class="help-block">:message</p>') !!}-->
        </div>

    </div>
    <div class="col-md-6">
        <div class=" {{ $errors->has('user_status') ? 'has-error' : ''}}">
            <label for="user_status" class="control-label">{{ 'Status' }}</label>
            {!! Form::select('user_status', ['1'=>'Active','0'=>'Inactive'], isset($teacher->user_status) ?$teacher->user_status : null, ['class' => 'form-control']) !!}
<!--            {!! $errors->first('user_status', '<p class="help-block">:message</p>') !!}-->
        </div>

    </div>
    <div class="col-md-6">
        <!--        <div class=" {{ $errors->has('user_url') ? 'has-error' : ''}}">
                    <label for="user_url" class="control-label">{{ 'User Url' }}</label>
                    {!! Form::text('user_url', isset($parent->user_url) ? $parent->user_url : null, ['class' => 'form-control']) !!}
                    {!! $errors->first('user_url', '<p class="help-block">:message</p>') !!}
                </div>-->

    </div>
</div>
<div class="col-md-12">

    <div class="col-md-4">
        <div class=" {{ $errors->has('name') ? 'has-error' : ''}}">
            <label for="name" class="control-label">{{ 'Levels' }}</label>

            {!! Form::select('level_id[]', \App\models\Level::pluck('name', 'id'),isset($teacher->levels) ?$teacher->levels : null, ['class' => 'form-control','multiple'=>true]) !!}
<!--            {!! $errors->first('level_id[]', '<p class="help-block">:message</p>') !!}-->
        </div>

    </div>


</div>
<div class="col-md-12" style="margin-top: 10px;margin-left: 14px;">
    <a href="{{ url('/teacher') }}" title="Back"><span class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</span></a>
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>