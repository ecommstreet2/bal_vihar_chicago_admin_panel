@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
          <div class="card-header"> <h4 style=""><span>Teacher {{ $teacher->display_name}}</span></h4></div>
        <div class="col-md-12">
            <div class="card">
                
                <div class="card-body">

                    <a href="{{ url('/teacher') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> </button></a>
                    <a href="{{ url('/teacher/' . $teacher->ID. '/edit') }}" title="Edit Teacher"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>
                    {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['teacher', $teacher->id],
                    'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-sm',
                    'title' => 'Delete Teacher',
                    'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                    {!! Form::close() !!}
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <th>ID</th><td>{{ $teacher->ID}}</td>
                                </tr>
                                <tr><th> Nick Name </th><td> {{ $teacher->user_nicename }} </td></tr>
                                <tr><th> Display Name </th><td> {{ $teacher->display_name }} </td></tr>
                                <tr><th>Email </th><td> {{ $teacher->user_email }} </td></tr>
                                <tr><th> Status </th><td> {{ ($teacher->user_status==1)?'Active':'Inactive' }} </td></tr>
                                <tr><th> Created At </th><td> {{ date('m/d/Y',strtotime($teacher->user_registered)) }} </td></tr>


                            </tbody>
                        </table>
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <td colspan="4"><h5><strong>LEVEL INFORMATION</strong></h5></td>
                                </tr>


                                <?php
                                if (($teacher->levels)) {
                                    foreach ($teacher->levels as $key => $lvl) {
                                        if ($lvl) {
                                            ?>
                                            <tr>
                                                <th>ID</th><td>{{ $lvl->id }}</td>
                                                <th>Name </th><td>{{ $lvl->name }}</td>

                                            </tr>

                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
