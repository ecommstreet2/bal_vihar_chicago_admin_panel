@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Unauthorized</div>
                <div style="" class="card-body"> You can not access this page! This is for only {{ $role}}</div>



            </div>
        </div>
    </div>
</div>
@endsection
