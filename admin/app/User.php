<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable {

    use Notifiable;

    const ADMIN_ROLE = 'ADMIN';
    const TEACHER_ROLE = 'TEACHER';
    const PARENT_ROLE = 'PARENTS';

    protected $table = 'wp_users';
    protected $primaryKey = 'ID';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_login', 'user_pass', 'user_nicename', 'user_email', 'user_url', 'user_registered', 'user_activation_key', 'user_status', 'display_name', 'remember_token', 'email_verified_at', 'user_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_pass', 'remember_token',
    ];
    public $timestamps = false;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function userMeta() {
        return $this->hasMany('App\models\UserMeta');
    }

    public function isAdmin() {
        return $this->user_type == self::ADMIN_ROLE;
    }

    public function isTeacher() {
        return $this->user_type == self::TEACHER_ROLE;
    }

}
