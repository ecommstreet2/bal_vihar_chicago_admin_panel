<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Library extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wp_library';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['pdf', 'level', 'status', 'teacher_id','display_name'];

    public function levels() {
        return $this->hasOne('App\models\Level', 'id', 'level');
    }

}
