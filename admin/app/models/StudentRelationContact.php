<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class StudentRelationContact extends Model {

    protected $table = 'wp_relationcontact';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'parent_id', 'first_name', 'last_name', 'mobile', 'relation'
    ];

    public function parent() {
    return $this->belongsTo('App\models\Parents', 'id', 'parent_id');


    }

}
