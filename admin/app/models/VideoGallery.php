<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class VideoGallery extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wp_video_galleries';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['gallery_id', 'name', 'type', 'mime_type', 'title', 'video', 'created_at', 'updated_at', 'status'];

}
