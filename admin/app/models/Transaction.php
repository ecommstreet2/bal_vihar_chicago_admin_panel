<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wp_transaction';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['transaction_id', 'parent_id', 'type', 'amount', 'status', 'created_at', 'updated_at'];

    public function parent()
    {
    return $this->belongsTo('App\models\Parents', 'parent_id', 'id');

    }

}
