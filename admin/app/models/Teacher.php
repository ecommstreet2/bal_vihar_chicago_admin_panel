<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model {

    protected $table = 'wp_users';
    protected $primaryKey = 'ID';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_login', 'user_pass', 'user_nicename', 'user_email', 'user_url', 'user_registered', 'user_activation_key', 'user_status', 'display_name', 'remember_token', 'email_verified_at', 'user_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_pass', 'remember_token',
    ];
    public $timestamps = false;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeTeacher($query) {
        return $query->where('user_type', '=', 'TEACHER');
    }

    public function levels() {
        return $this->belongsToMany(Level::class, 'wp_users_level', 'user_id', 'level_id');
    }

    public function userMeta() {
        return $this->hasMany('App\models\UserMeta');
    }

}
