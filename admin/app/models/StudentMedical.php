<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class StudentMedical extends Model {

    protected $table = 'wp_studentmedical';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['parent_id', 'name', 'email', 'phone', 'address'];
   public $timestamps = false;
    public function parent() {
    return $this->belongsTo('App\models\Parents', 'id', 'parent_id');




    }

}
