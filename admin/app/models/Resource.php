<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wp_resources';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['weblink_url', 'level', 'teacher_id','title'];

    public function levels() {
        return $this->hasOne('App\models\Level', 'id', 'level');
    }

}
