<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class LatestUpdate extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wp_latest_updates';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['update_date', 'description', 'status', 'title', 'level'];

    public function levels() {
        return $this->hasOne('App\models\Level', 'id', 'level');
    }

}
