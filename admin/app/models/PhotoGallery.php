<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class PhotoGallery extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wp_photo_galleries';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['gallery_id', 'thumbnail','name', 'type', 'title', 'image', 'created_at', 'updated_at', 'status'];

}
