<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class NewsMeta extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wp_postmeta';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'meta_id';
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['post_id', 'meta_key', 'meta_value'];

}
