<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Parents extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wp_parents';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['parent_email', 'user_id', 'mother_first_name', 'mother_last_name', 'fother_first_name', 'fother_last_name', 'address', 'city', 'state', 'zip', 'home_phone_number', 'mother_mobile_number', 'father_mobile_number', 'transaction_id'];

    public function student() {
        return $this->hasMany('App\models\Student', 'parent_id', 'id');
    }

    public function studentMedical() {
        return $this->hasOne('App\models\StudentMedical', 'parent_id', 'id');
    }

    public function studentRelationContact() {
        return $this->hasMany('App\models\StudentRelationContact', 'parent_id', 'id');
    }

    public function levels() {
        return $this->belongsToMany(Level::class, 'wp_parents_level', 'parent_id', 'level_id');
    }

    public function user() {
        return $this->belongsTo('App\User', 'ID', 'user_id');
    }

}
