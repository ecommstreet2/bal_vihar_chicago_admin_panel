<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class AcademicCalender extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wp_posts';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'ID';
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['post_author', 'post_date', 'post_date_gmt', 'post_content', 'post_title','post_excerpt','post_status','comment_status','ping_status','post_password','post_name','to_ping',
        'pinged','post_modified','post_modified_gmt','post_content_filtered','post_parent','guid','menu_order','post_type','post_mime_type','comment_count'];

}
