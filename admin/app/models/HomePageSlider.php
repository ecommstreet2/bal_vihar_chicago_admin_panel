<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class HomePageSlider extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wp_slider';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['post_title', 'post_date', 'slider_image', 'destination_link', 'post_content','link_title'];

}
