<?php

namespace App\models;

use App\models\Parents;
use Illuminate\Database\Eloquent\Model;

class Level extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'levels';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug'];

    public function parents() {

        return $this->belongsToMany(Parents::class, 'wp_parents_level', 'parent_id', 'level_id');
    }

}
