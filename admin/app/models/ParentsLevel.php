<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class ParentsLevel extends Model {

    protected $table = 'wp_parents_level';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['parent_id', 'level_id'];
    public $timestamps = false;

}
