<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wp_events';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['event_name', 'event_description', 'event_image', 'event_location', 'event_start_date', 'event_start_time', 'event_end_date', 'event_end_time', 'is_active', 'created_at', 'updated_at', 'status'];

}
