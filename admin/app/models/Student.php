<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model {

    protected $table = 'wp_students';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'birth_date', 'apply_level', 'parent_id', 'updated_at'];

    public function parent() {
    return $this->belongsTo('App\models\Parents', 'id', 'parent_id');


    }

}
