<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Karyakarta extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wp_karyakarta';
    public $timestamps = false;

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'education', 'designation', 'description', 'phone', 'address', 'email', 'image', 'status','created_at'];

}
