<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class UsersLevel extends Model {

    protected $table = 'wp_users_level';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'level_id'];
    public $timestamps = false;

}
