<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use MikeMcLin\WpPassword\Facades\WpPassword;
use Auth;

class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    protected function attemptLogin(Request $request) {

        $user = \App\User::where([
                    'user_login' => $request->email,
                        //  'password' => md5($request->password)
                ])->first();

        if ($user) {
            if (WpPassword::check($request->password, $user->user_pass)) {
             
                $this->guard()->login($user, $request->has('remember'));
                  
                return true;
            }

            return false;
        }

        return false;
    }

}
