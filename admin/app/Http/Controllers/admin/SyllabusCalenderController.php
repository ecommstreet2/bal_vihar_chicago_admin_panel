<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\models\SyllabusCalender;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SyllabusCalenderController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $syllabusCalender = SyllabusCalender::where('date', 'LIKE', "%$keyword%")
                            ->orWhere('description', 'LIKE', "%$keyword%")
                            ->latest('id')->paginate($perPage);
        } else {
            $syllabusCalender = SyllabusCalender::latest('id')->paginate($perPage);
        }

        return view('admin.syllabus-calender.index', compact('syllabusCalender'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.syllabus-calender.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'description' => 'required','title' => 'required',
                    'calender_date' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('syllabus-calender/create')
                            ->withErrors($validator)
                            ->withInput();
        }
        $requestData = $request->all();
        $requestData['calender_date'] = date('Y-m-d H:i:s', strtotime($request->calender_date));
        SyllabusCalender::create($requestData);

        return redirect('syllabus-calender')->with('flash_message', 'SyllabusCalender added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $syllabusCalender = SyllabusCalender::findOrFail($id);

        return view('admin.syllabus-calender.show', compact('syllabusCalender'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $syllabusCalender = SyllabusCalender::findOrFail($id);

        return view('admin.syllabus-calender.edit', compact('syllabusCalender'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {

        $requestData = $request->all();

        $syllabusCalender = SyllabusCalender::findOrFail($id);
        $validator = Validator::make($request->all(), [
                    'description' => 'required', 'title' => 'required',
                    'calender_date' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('syllabus-calender/' . $id . '/edit')
                            ->withErrors($validator)
                            ->withInput();
        }
        $requestData['calender_date'] = date('Y-m-d H:i:s', strtotime($request->calender_date));
        $syllabusCalender->update($requestData);

        return redirect('syllabus-calender')->with('flash_message', 'SyllabusCalender updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        SyllabusCalender::destroy($id);

        return redirect('syllabus-calender')->with('flash_message', 'SyllabusCalender deleted!');
    }

}
