<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\models\Library;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;

class LibraryController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $library = Library::query();
            if (!Auth::user()->isAdmin()) {
                $library = $library->where('teacher_id', Auth::user()->ID);
            }
            $library = $library->where('pdf', 'LIKE', "%$keyword%")
                            ->orWhere('level', 'LIKE', "%$keyword%")
                            ->latest('id')->paginate($perPage);
        } else {

            $library = Library::query();
            if (!Auth::user()->isAdmin()) {
                $library = $library->where('teacher_id', Auth::user()->ID);
            }
            $library = $library->latest('id')->paginate($perPage);
        }

        return view('admin.library.index', compact('library'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.library.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {

        $validator = Validator::make($request->all(), [


                    'level' => 'required',
                    'pdf' => 'required|file|mimes:pdf',
                    'display_name' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('library/create')
                            ->withErrors($validator)
                            ->withInput();
        }
        $requestData = $request->all();
        if (isset($request->pdf) && !empty($request->pdf)) {
            $pdfName = time() . '.' . $request->pdf->extension();
            $requestData['pdf'] = asset('uploads/' . $pdfName);
            $request->pdf->move(public_path('uploads'), $pdfName);
        }

        if (!Auth::user()->isAdmin()) {

            $requestData['teacher_id'] = Auth::user()->ID;
        }

        Library::create($requestData);

        return redirect('library')->with('flash_message', 'Library added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $library = Library::findOrFail($id);

        return view('admin.library.show', compact('library'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $library = Library::findOrFail($id);

        return view('admin.library.edit', compact('library'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {

        $requestData = $request->all();

        $library = Library::findOrFail($id);
        $validator = Validator::make($request->all(), [


                    'level' => 'required',
                    'pdf' => 'mimes:pdf',
                    'display_name' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('library/' . $id . '/edit')
                            ->withErrors($validator)
                            ->withInput();
        }

        if (isset($request->pdf) && !empty($request->pdf)) {
            if (file_exists(public_path('uploads') . '/' . $library->pdf)) {
                File::delete(public_path('uploads') . '/' . $library->pdf);
            }
            $pdfName = time() . '.' . $request->pdf->extension();
            $requestData['pdf'] = asset('uploads/' . $pdfName);
            $request->pdf->move(public_path('uploads'), $pdfName);
        } else {
            $requestData['pdf'] = $library->pdf;
        }
        if (!Auth::user()->isAdmin()) {
            $requestData['teacher_id'] = Auth::user()->ID;
        }
        $library->update($requestData);

        return redirect('library')->with('flash_message', 'Library updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        Library::destroy($id);

        return redirect('library')->with('flash_message', 'Library deleted!');
    }

}
