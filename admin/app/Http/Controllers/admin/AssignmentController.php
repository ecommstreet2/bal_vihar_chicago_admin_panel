<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\models\Assignment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;

class AssignmentController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $assignment = Assignment::query();
            if (!Auth::user()->isAdmin()) {
                $assignment = $assignment->where('teacher_id', Auth::user()->ID);
            }
            $assignment = $assignment->where('pdf', 'LIKE', "%$keyword%")
                            ->orWhere('level', 'LIKE', "%$keyword%")
                            ->latest('id')->paginate($perPage);
        } else {

            $assignment = Assignment::query();
            if (!Auth::user()->isAdmin()) {
                $assignment = $assignment->where('teacher_id', Auth::user()->ID);
            }
            $assignment = $assignment->latest('id')->paginate($perPage);
        }

        return view('admin.assignment.index', compact('assignment'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.assignment.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [


                    'level' => 'required',
                    'pdf' => 'required|file|mimes:pdf',
                    'display_name' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('assignment/create')
                            ->withErrors($validator)
                            ->withInput();
        }
        $requestData = $request->all();
        if (isset($request->pdf) && !empty($request->pdf)) {
            $pdfName = time() . '.' . $request->pdf->extension();
            $requestData['pdf'] =  asset('uploads/' . $pdfName);;
            $request->pdf->move(public_path('uploads'), $pdfName);
        }
        if (!Auth::user()->isAdmin()) {
            $requestData['teacher_id'] = Auth::user()->ID;
        }
        Assignment::create($requestData);

        return redirect('assignment')->with('flash_message', 'Assignment added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $assignment = Assignment::findOrFail($id);

        return view('admin.assignment.show', compact('assignment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $assignment = Assignment::findOrFail($id);

        return view('admin.assignment.edit', compact('assignment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {

        $requestData = $request->all();

        $assignment = Assignment::findOrFail($id);

        $validator = Validator::make($request->all(), [


                    'level' => 'required',
                    'pdf' => 'file|mimes:pdf',
                    'display_name' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('assignment/' . $id . '/edit')
                            ->withErrors($validator)
                            ->withInput();
        }

        if (isset($request->pdf) && !empty($request->pdf)) {
            if (file_exists(public_path('uploads') . '/' . $assignment->pdf)) {
                File::delete(public_path('uploads') . '/' . $assignment->pdf);
            }
            $pdfName = time() . '.' . $request->pdf->extension();
            $requestData['pdf'] = asset('uploads/' . $pdfName);
            $request->pdf->move(public_path('uploads'), $pdfName);
        } else {
            $requestData['pdf'] = $assignment->pdf;
        }
        if (!Auth::user()->isAdmin()) {
            $requestData['teacher_id'] = Auth::user()->ID;
        }
        $assignment->update($requestData);

        return redirect('assignment')->with('flash_message', 'Assignment updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        Assignment::destroy($id);

        return redirect('assignment')->with('flash_message', 'Assignment deleted!');
    }

}
