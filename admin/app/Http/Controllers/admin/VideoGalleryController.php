<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\models\VideoGallery;
use App\models\PhotoGallery;
use App\models\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class VideoGalleryController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

//        if (!empty($keyword)) {
//            $videogallery = VideoGallery::where('name', 'LIKE', "%$keyword%")
//                ->orWhere('type', 'LIKE', "%$keyword%")
//                ->orWhere('created_at', 'LIKE', "%$keyword%")
//                ->orWhere('updated_at', 'LIKE', "%$keyword%")
//                ->orWhere('status', 'LIKE', "%$keyword%")
//                ->latest()->paginate($perPage);
//        } else {
//            $videogallery = VideoGallery::latest()->paginate($perPage);
//        }
        if (!empty($keyword)) {
            $videogallery = Gallery::where('type', 'VIDEO')
                            ->where(function($q) use ($keyword) {
                                $q->where('name', 'LIKE', "%$keyword%")
                                ->orWhere('type', 'LIKE', "%$keyword%")
                                ->orWhere('created_at', 'LIKE', "%$keyword%")
                                ->orWhere('updated_at', 'LIKE', "%$keyword%")
                                ->orWhere('status', 'LIKE', "%$keyword%");
                            })
                            ->latest()->paginate($perPage);
        } else {
            $videogallery = Gallery::where('type', 'VIDEO')
                            ->latest()->paginate($perPage);
        }

        return view('admin.video-gallery.index', compact('videogallery'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request) {
        $gallery_id = $request->get('id');
        $perPage = 25;
        $videogallies = VideoGallery::where('gallery_id', $gallery_id)
                        ->latest()->paginate($perPage);

        return view('admin.video-gallery.create', compact('gallery_id', 'videogallies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {

        $validator = Validator::make($request->all(), [

                    'name' => 'required',
                    'video' => 'required',
                        //'video' => 'mimes:mp4,mov,ogg,qt',
        ]);
        $validator->after(function ($validator)use($request) {
            if ($request->hasFile('video')) {
                $videos = $request->file('video');
                foreach ($videos as $key => $vido) {
                    $extention = strtolower($vido->getClientOriginalExtension());

                    if ($extention != 'm4v' && $extention != 'mp4' && $extention != 'mov' && $extention != '3gp' && $extention != 'flv' && $extention != 'ogg') {
                        $validator->errors()->add('video', 'The video must be a file of type: mp4, mov, etc.........');
                    }
                }
            }
        });
        if ($validator->fails()) {
            return redirect('video-gallery/create?id=' . $request->gallery_id)
                            ->withErrors($validator)
                            ->withInput();
        }
        $requestData = $request->all();

        if ($request->hasFile('video')) {
            $videos = $request->file('video');
            $folderName = Str::slug($request->name . '_' . $request->gallery_id, '-');
            $org_img = $thm_img = true;

            if (!File::exists('gallery/' . $folderName . '/video/originals/')) {
                $org_img = File::makeDirectory(public_path('gallery/' . $folderName . '/video/originals/'), 0777, true);
            }
            if (!File::exists('gallery/' . $folderName . '/video/thumbnails')) {
                $thm_img = File::makeDirectory(public_path('gallery/' . $folderName . '/video/thumbnails'), 0777, true);
            }

            foreach ($videos as $key => $vido) {

                $gallery = new VideoGallery();

                $filename = rand(1111, 9999) . time() . '.' . $vido->getClientOriginalExtension();

                $org_path = 'gallery/' . $folderName . '/video/originals/';
                $thm_path = 'gallery/' . $folderName . '/video/thumbnails/';
                $gallery->video = 'gallery/' . $folderName . '/video/originals/' . $filename;
                $gallery->status = 1;
                $gallery->type = 'VIDEO';
                $gallery->mime_type = $vido->getClientMimeType();

                $gallery->title = $request->name;
                $gallery->name = $request->name;
                $gallery->gallery_id = $request->gallery_id;
                $gallery->created_at = date('Y-m-d H:i:s');

                if (!$gallery->save()) {
                    flash('Gallery could not be updated.')->error()->important();
                    return redirect('video-gallery/create?id=' . $request->gallery_id)->withInput();
                }
                $vido->move(public_path() . '/' . $org_path, $filename);
            }
        }


        return redirect('video-gallery/create?id=' . $request->gallery_id)->with('flash_message', 'PhotoGallery added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
//    public function show($id) {
//        $videogallery = VideoGallery::findOrFail($id);
//
//        return view('admin.video-gallery.show', compact('videogallery'));
//    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
//    public function edit($id) {
//        $videogallery = VideoGallery::findOrFail($id);
//
//        return view('admin.video-gallery.edit', compact('videogallery'));
//    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
//    public function update(Request $request, $id) {
//
//        $requestData = $request->all();
//
//        $videogallery = VideoGallery::findOrFail($id);
//        $videogallery->update($requestData);
//
//        return redirect('video-gallery')->with('flash_message', 'VideoGallery updated!');
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        VideoGallery::destroy($id);

        return redirect('video-gallery')->with('flash_message', 'VideoGallery deleted!');
    }

    public function createGallery(Request $request) {
        $validator = Validator::make($request->all(), [

                    'name' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('video-gallery')
                            ->withErrors($validator)
                            ->withInput();
        }
        $requestData = $request->all();
        $requestData['type'] = 'VIDEO';
        $requestData['status'] = 1;
        $requestData['created_at'] = date('Y-m-d H:i:s');
        Gallery::create($requestData);

        return redirect('video-gallery')->with('flash_message', 'Gallery added!');
    }

}
