<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\models\AcademicCalender;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AcademicCalenderController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $academicCalender = AcademicCalender::where('post_type', 'event')->where('date', 'LIKE', "%$keyword%")
                            ->orWhere('description', 'LIKE', "%$keyword%")
                            ->latest('id')->paginate($perPage);
        } else {
            $academicCalender = AcademicCalender::where('post_type', 'event')->latest('id')->paginate($perPage);
        }

        return view('admin.academic-calender.index', compact('academicCalender'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.academic-calender.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $createdAt = time();
        $validator = Validator::make($request->all(), [
                    'event_name' => 'required',
                    'event_description' => 'required',
                    //   'event_location_name' => 'required',
                    'event_start_date' => 'required|date|date_format:m/d/Y',
                    'event_start_time' => 'required|date_format:H:i:s',
                    'event_end_date' => 'nullable|date|date_format:m/d/Y|after_or_equal:event_start_date',
                    'event_end_time' => 'nullable|date_format:H:i:s',
                    'event_image' => 'image|mimes:jpeg,png,jpg,gif,svg',
//                    'event_address' => 'required',  'event_city' => 'required',
//                      'event_state' => 'required','event_country' => 'required',  'event_zip' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('academic-calender/create')
                            ->withErrors($validator)
                            ->withInput();
        }
        $requestData = $request->all();

        if (isset($request->event_image) && !empty($request->event_image)) {
            $imageName = time() . '.' . $request->event_image->extension();
            $requestData['guid'] = asset('uploads/' . $imageName);
            $request->event_image->move(public_path('uploads'), $imageName);
        }

        $requestData['post_date'] = date('Y-m-d H:i:s', $createdAt);
        $requestData['post_date_gmt'] = gmdate('Y-m-d H:i:s', $createdAt);
        $requestData['post_modified'] = date('Y-m-d H:i:s', $createdAt);
        $requestData['post_modified_gmt'] = gmdate('Y-m-d H:i:s', $createdAt);
        $requestData['post_author'] = Auth::user()->ID;
        $requestData['post_status'] = 'publish';
        $requestData['comment_status'] = 'open';
        $requestData['ping_status'] = 'closed';
        $requestData['post_name'] = Str::slug($request->event_name, '-');
        $requestData['post_type'] = 'event';
        $requestData['post_excerpt'] = '';
        $requestData['to_ping'] = '';
        $requestData['post_content'] = $request->event_description;
        $requestData['post_title'] = $request->event_name;
        $requestData['pinged'] = '';
        $requestData['post_content_filtered'] = '';
        $wp_post_event = AcademicCalender::create($requestData);


        $wp_posts_location_id = DB::table('wp_posts')->insertGetId(
                [ 'post_author' => Auth::user()->ID, 'post_date' => date('Y-m-d H:i:s', $createdAt), 'post_date_gmt' => gmdate('Y-m-d H:i:s', $createdAt),
                    'post_title' => Str::slug($request->event_location_name, '-'), 'post_status' => 'publish', 'comment_status' => 'open', 'ping_status' => 'closed',
                    'post_name' => Str::slug($request->event_location_name, '-'), 'post_modified' => date('Y-m-d H:i:s', $createdAt), 'post_modified_gmt' => gmdate('Y-m-d H:i:s', $createdAt),
                    'post_type' => 'location', 'post_excerpt' => '', 'to_ping' => '', 'pinged' => '', 'post_content_filtered' => '', 'post_content' => $request->event_location_name
                ]
        );

        $wp_em_locations_id = DB::table('wp_em_locations')->insertGetId(
                [ 'post_id' => isset($wp_posts_location_id) ? $wp_posts_location_id : NULL, 'location_slug' => Str::slug($request->event_location_name, '-'), 'location_name' => $request->event_location_name,
                    'location_owner' => Auth::user()->ID, 'location_address' => $requestData['event_address'], 'location_town' => $requestData['event_city'],
                    'location_postcode' => $requestData['event_zip'], 'location_region' => $requestData['event_state'], 'location_country' => $requestData['event_country'],
                    'location_status' => 1, 'location_language' => 'en_US',
                ]
        );

        $wp_em_events_id = DB::table('wp_em_events')->insertGetId(
                [ 'post_id' => $wp_post_event->ID, 'event_slug' => Str::slug($request->event_name, '-'),
                    'event_owner' => 1, 'event_status' => 1, 'event_name' => $request->event_name,
                    'event_start_date' => date('Y-m-d', strtotime($request->event_start_date)), 'event_end_date' => date('Y-m-d', strtotime($request->event_end_date)),
                    'event_start_time' => date('H:i:s', strtotime($request->event_start_time)), 'event_end_time' => date('H:i:s', strtotime($request->event_end_time)),
                    'event_start' => date('Y-m-d', strtotime($request->event_start_date)) . ' ' . date('H:i:s', strtotime($request->event_start_time)),
                    'event_end' => date('Y-m-d', strtotime($request->event_end_date)) . ' ' . date('H:i:s', strtotime($request->event_end_time)),
                    'event_timezone' => 'UTC', 'event_language' => 'en_US',
                    'event_date_created' => date('Y-m-d H:i:s', $createdAt), 'location_id' => isset($wp_em_locations_id) ? $wp_em_locations_id : NULL,
                ]
        );



        return redirect('academic-calender')->with('flash_message', 'Academic Calender added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $academicCalender = AcademicCalender::findOrFail($id);

        return view('admin.academic-calender.show', compact('academicCalender'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $academicCalender = AcademicCalender::findOrFail($id);

        return view('admin.academic-calender.edit', compact('academicCalender'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {

        $requestData = $request->all();
        $createdAt = time();
        $academicCalender = AcademicCalender::findOrFail($id);
        $validator = Validator::make($request->all(), [
                    'event_name' => 'required',
                    'event_description' => 'required',
                    'event_location' => 'required',
                    'event_start_date' => 'required|date|date_format:m/d/Y',
                    'event_start_time' => 'required|date_format:H:i:s',
                    'event_end_date' => 'nullable|date|date_format:m/d/Y|after_or_equal:event_start_date',
                    'event_end_time' => 'nullable|date_format:H:i:s',
                    'event_image' => 'image|mimes:jpeg,png,jpg,gif,svg',
//                    'event_address' => 'required',  'event_city' => 'required',
//                      'event_state' => 'required','event_country' => 'required',  'event_zip' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('academic-calender/' . $id . '/edit')
                            ->withErrors($validator)
                            ->withInput();
        }
        if (isset($request->event_image) && !empty($request->event_image)) {
            $imageName = time() . '.' . $request->event_image->extension();

            $requestData['guid'] = asset('uploads/' . $imageName);
            $request->event_image->move(public_path('uploads'), $imageName);
        }
        $requestData['event_start_date'] = date('Y-m-d H:i:s', strtotime($request->event_start_date));
        $requestData['event_end_date'] = date('Y-m-d H:i:s', strtotime($request->event_end_date));




        $requestData['post_date'] = date('Y-m-d H:i:s', strtotime($request->post_date));
        $requestData['post_date_gmt'] = gmdate('Y-m-d H:i:s', strtotime($request->post_date));
        $requestData['post_modified'] = date('Y-m-d H:i:s', $createdAt);
        $requestData['post_modified_gmt'] = gmdate('Y-m-d H:i:s', $createdAt);


        $requestData['post_author'] = Auth::user()->ID;
        $requestData['post_status'] = 'publish';
        $requestData['comment_status'] = 'open';
        $requestData['ping_status'] = 'closed';
        $requestData['post_name'] = Str::slug($request->post_title, '-');
        $requestData['post_type'] = 'news';
        $requestData['post_excerpt'] = '';
        $requestData['to_ping'] = '';
        $requestData['pinged'] = '';
        $requestData['post_content_filtered'] = '';

        $academicCalender->update($requestData);

        return redirect('academic-calender')->with('flash_message', 'AcademicCalender updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        AcademicCalender::destroy($id);

        return redirect('academic-calender')->with('flash_message', 'AcademicCalender deleted!');
    }

}
