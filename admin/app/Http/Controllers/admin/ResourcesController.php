<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\models\Resource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class ResourcesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $resources = Resource::query();
            if (!Auth::user()->isAdmin()) {
                $resources = $resources->where('teacher_id', Auth::user()->ID);
            }
            $resources = $resources->where('weblink_url', 'LIKE', "%$keyword%")
                            ->orWhere('level', 'LIKE', "%$keyword%")
                            ->latest('id')->paginate($perPage);
        } else {
            $resources = Resource::query();
            if (!Auth::user()->isAdmin()) {
                $resources = $resources->where('teacher_id', Auth::user()->ID);
            }
            $resources = $resources->latest('id')->paginate($perPage);
        }
        if (!Auth::user()->isAdmin()) {
            $requestData['teacher_id'] = Auth::user()->ID;
        }
        return view('admin.resources.index', compact('resources'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.resources.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {

        $validator = Validator::make($request->all(), [

                    'weblink_url' => 'required|url',
                    'level' => 'required',
                    'title' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('resources/create')
                            ->withErrors($validator)
                            ->withInput();
        }
        $requestData = $request->all();
        if (!Auth::user()->isAdmin()) {
            $requestData['teacher_id'] = Auth::user()->ID;
        }
        Resource::create($requestData);

        return redirect('resources')->with('flash_message', 'Resource added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $resource = Resource::findOrFail($id);

        return view('admin.resources.show', compact('resource'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $resource = Resource::findOrFail($id);

        return view('admin.resources.edit', compact('resource'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {


        $requestData = $request->all();

        $resource = Resource::findOrFail($id);
        $validator = Validator::make($request->all(), [

                    'weblink_url' => 'required|url',
                    'level' => 'required',
                    'title' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('resources/' . $id . '/edit')
                            ->withErrors($validator)
                            ->withInput();
        }
        if (!Auth::user()->isAdmin()) {
            $requestData['teacher_id'] = Auth::user()->ID;
        }
        $resource->update($requestData);

        return redirect('resources')->with('flash_message', 'Resource updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        Resource::destroy($id);

        return redirect('resources')->with('flash_message', 'Resource deleted!');
    }

}
