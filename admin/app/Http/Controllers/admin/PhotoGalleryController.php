<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\models\PhotoGallery;
use App\models\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class PhotoGalleryController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

//        if (!empty($keyword)) {
//            $photogallery = PhotoGallery::where('name', 'LIKE', "%$keyword%")
//                            ->orWhere('type', 'LIKE', "%$keyword%")
//                            ->orWhere('created_at', 'LIKE', "%$keyword%")
//                            ->orWhere('updated_at', 'LIKE', "%$keyword%")
//                            ->orWhere('status', 'LIKE', "%$keyword%")
//                            ->latest()->paginate($perPage);
//        } else {
//            $photogallery = PhotoGallery::latest()->paginate($perPage);
//        }
        if (!empty($keyword)) {
            $photogallery = Gallery::where('type', 'PHOTO')
                            ->where(function($q) use ($keyword) {
                                $q->where('name', 'LIKE', "%$keyword%")
                                ->orWhere('type', 'LIKE', "%$keyword%")
                                ->orWhere('created_at', 'LIKE', "%$keyword%")
                                ->orWhere('updated_at', 'LIKE', "%$keyword%")
                                ->orWhere('status', 'LIKE', "%$keyword%");
                            })
                            ->latest()->paginate($perPage);
        } else {
            $photogallery = Gallery::where('type', 'PHOTO')
                            ->latest()->paginate($perPage);
        }
        return view('admin.photo-gallery.index', compact('photogallery'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request) {
        $gallery_id = $request->get('id');
        $perPage = 25;
        $photogallies = PhotoGallery::where('gallery_id', $gallery_id)
                        ->latest()->paginate($perPage);

        return view('admin.photo-gallery.create', compact('gallery_id', 'photogallies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {

        $validator = Validator::make($request->all(), [

                  //  'name' => 'required',
                    'image' => 'required',
                        // 'image' => 'mimes:jpeg,JPEG,png,PNG,jpg,JPG,gif,GIF,svg',
        ]);
        $validator->after(function ($validator)use($request) {
            if ($request->hasFile('image')) {
                $images = $request->file('image');
                foreach ($images as $key => $imag) {
                    $extention = strtolower($imag->getClientOriginalExtension());

                    if ($extention != 'jpeg' && $extention != 'png' && $extention != 'jpg' && $extention != 'gif' && $extention != 'svg') {
                        $validator->errors()->add('image', 'The video must be a file of type: jpeg, png,gif etc.........');
                    }
                }
            }
        });
        if ($validator->fails()) {
            return redirect('photo-gallery/create?id=' . $request->gallery_id)
                            ->withErrors($validator)
                            ->withInput();
        }
        $requestData = $request->all();

        if ($request->hasFile('image')) {
            $images = $request->file('image');
            $folderName = Str::slug($request->name . '_' . $request->gallery_id, '-');
            $org_img = $thm_img = true;

            if (!File::exists('gallery/' . $folderName . '/originals/')) {
                $org_img = File::makeDirectory(public_path('gallery/' . $folderName . '/originals/'), 0777, true);
            }
            if (!File::exists('gallery/' . $folderName . '/thumbnails/')) {
                $thm_img = File::makeDirectory(public_path('gallery/' . $folderName . '/thumbnails'), 0777, true);
            }

            foreach ($images as $key => $image) {

                $gallery = new PhotoGallery();

                $filename = rand(1111, 9999) . time() . '.' . $image->getClientOriginalExtension();

                $org_path = 'gallery/' . $folderName . '/originals/' . $filename;
                $thm_path = 'gallery/' . $folderName . '/thumbnails/' . $filename;

                $gallery->image = 'gallery/' . $folderName . '/originals/' . $filename;
                $gallery->thumbnail = 'gallery/' . $folderName . '/thumbnails/' . $filename;
                $gallery->status = 1;
                $gallery->type = 'PHOTO';
                $gallery->title = $request->name;
                $gallery->name = $request->name;
                $gallery->gallery_id = $request->gallery_id;
                $gallery->created_at = date('Y-m-d H:i:s');

                if (!$gallery->save()) {
                    flash('Gallery could not be updated.')->error()->important();
                    return redirect('photo-gallery/create?id=' . $request->gallery_id)->withInput();
                }

                if (($org_img && $thm_img) == true) {
                    Image::make($image)->fit(900, 500, function ($constraint) {
                        $constraint->upsize();
                    })->save($org_path);
                    Image::make($image)->fit(270, 160, function ($constraint) {
                        $constraint->upsize();
                    })->save($thm_path);
                }
            }
        }


        return redirect('photo-gallery/create?id=' . $request->gallery_id)->with('flash_message', 'PhotoGallery added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $photogallery = PhotoGallery::findOrFail($id);

        return view('admin.photo-gallery.show', compact('photogallery'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $photogallery = PhotoGallery::findOrFail($id);

        return view('admin.photo-gallery.edit', compact('photogallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {

        $requestData = $request->all();

        $photogallery = PhotoGallery::findOrFail($id);
        $photogallery->update($requestData);

        return redirect('photo-gallery')->with('flash_message', 'PhotoGallery updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        PhotoGallery::destroy($id);

        return redirect('photo-gallery')->with('flash_message', 'PhotoGallery deleted!');
    }

    public function createGallery(Request $request) {
        $validator = Validator::make($request->all(), [

                 //   'name' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('photo-gallery')
                            ->withErrors($validator)
                            ->withInput();
        }
        $requestData = $request->all();
        $requestData['type'] = 'PHOTO';
        $requestData['status'] = 1;
        $requestData['created_at'] = date('Y-m-d H:i:s');
        Gallery::create($requestData);

        return redirect('photo-gallery')->with('flash_message', 'Gallery added!');
    }

}
