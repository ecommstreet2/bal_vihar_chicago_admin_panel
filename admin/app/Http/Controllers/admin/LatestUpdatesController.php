<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\models\LatestUpdate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LatestUpdatesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $latestupdates = LatestUpdate::where('date', 'LIKE', "%$keyword%")
                            ->orWhere('description', 'LIKE', "%$keyword%")
                            ->latest('id')->paginate($perPage);
        } else {
            $latestupdates = LatestUpdate::latest('id')->paginate($perPage);
        }

        return view('admin.latest-updates.index', compact('latestupdates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.latest-updates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'description' => 'required','title' => 'required',
                    'update_date' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('latest-updates/create')
                            ->withErrors($validator)
                            ->withInput();
        }
        $requestData = $request->all();
        $requestData['update_date'] = date('Y-m-d H:i:s', strtotime($request->update_date));
        LatestUpdate::create($requestData);

        return redirect('latest-updates')->with('flash_message', 'LatestUpdate added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $latestupdate = LatestUpdate::findOrFail($id);

        return view('admin.latest-updates.show', compact('latestupdate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $latestupdate = LatestUpdate::findOrFail($id);

        return view('admin.latest-updates.edit', compact('latestupdate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {

        $requestData = $request->all();

        $latestupdate = LatestUpdate::findOrFail($id);
        $validator = Validator::make($request->all(), [
                    'description' => 'required', 'title' => 'required',
                    'update_date' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('latest-updates/' . $id . '/edit')
                            ->withErrors($validator)
                            ->withInput();
        }
        $requestData['update_date'] = date('Y-m-d H:i:s', strtotime($request->update_date));
        $latestupdate->update($requestData);

        return redirect('latest-updates')->with('flash_message', 'LatestUpdate updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        LatestUpdate::destroy($id);

        return redirect('latest-updates')->with('flash_message', 'LatestUpdate deleted!');
    }

}
