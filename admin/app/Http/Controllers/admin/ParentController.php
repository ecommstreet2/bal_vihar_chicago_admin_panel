<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\models\Parents;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use MikeMcLin\WpPassword\Facades\WpPassword;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ParentController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $parent = Parents::where('name', 'LIKE', "%$keyword%")
                            ->orWhere('slug', 'LIKE', "%$keyword%")
                            ->latest()->paginate($perPage);
        } else {
            $parent = Parents::latest()->paginate($perPage);
        }

        return view('admin.parent.index', compact('parent'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request) {
        $id = isset($request->id) ? $request->id : 0;

        if ($id) {
            $parent = Parents::findOrFail($id);
            $user = new \App\User();
            $studentRelationContact = $parent->studentRelationContact;
            $student = $parent->student;
            $levels = $parent->levels;
            $studentMedical = $parent->studentMedical;
            $user->user_login = $parent->parent_email;
            $user->user_email = $parent->parent_email;
            $user->user_nicename = $parent->fother_first_name . ' ' . $parent->fother_last_name;
            $user->display_name = $parent->fother_first_name . ' ' . $parent->fother_last_name;
            $user->user_pass = $parent->father_mobile_number;
        } else {
            $parent = new Parents();
            $user = new \App\User();
            $studentRelationContact = [new \App\models\StudentRelationContact()];
            $student = [new \App\models\Student()];
            $studentMedical = new \App\models\StudentMedical();
            $levels = [new \App\models\Level()];
        }

        return view('admin.parent.create', compact('parent', 'user', 'studentRelationContact', 'student', 'studentMedical', 'levels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_email' => 'required|unique:wp_users|max:255',
                    'user_pass' => 'required',
                    'level_id' => 'required',
                    'display_name' => 'required',
                    'mother_first_name' => 'required',
                    'fother_first_name' => 'required',
                    'father_mobile_number' => 'required',
                    'transaction_id' => 'required',
        ]);

        if ($validator->fails()) {
            if (isset($request->parent_id) && !empty($request->parent_id)) {
                return redirect('parent/create?id=' . $request->parent_id)
                                ->withErrors($validator)
                                ->withInput();
            } else {
                return redirect('parent/create')
                                ->withErrors($validator)
                                ->withInput();
            }
        }
        $requestData = $request->all();

        if (isset($request->parent_id) && !empty($request->parent_id)) {
            $parent = Parents::find($request->parent_id);
            if ($parent) {
                $user = \App\User::create(
                                [ 'user_login' => $requestData['user_email'], 'user_pass' => WpPassword::make($requestData['user_pass']),
                                    'user_nicename' => $requestData['user_nicename'], 'user_email' => $requestData['user_email'], 'user_url' => '',
                                    'user_registered' => date('Y-m-d H:i:s'), 'user_status' => 1, 'display_name' => $requestData['display_name'],
                                    'user_type' => 'PARENTS'
                                ]
                );
                if ($user) {
                    DB::table('wp_usermeta')->insert(['user_id' => $user->ID, 'meta_key' => 'show_admin_bar_front', 'meta_value' => 'false']);
                    DB::table('wp_usermeta')->insert(['user_id' => $user->ID, 'meta_key' => 'wp_capabilities', 'meta_value' => 'a:1:{s:6:"parent";b:1;}']);
                    if (isset($requestData['level_id']) && is_array($requestData['level_id'])) {
                        \App\models\ParentsLevel::where('parent_id', $parent->id)->delete();
                        foreach ($requestData['level_id']as $key => $levelId) {
                            $student = \App\models\ParentsLevel::create(
                                            ['parent_id' => $parent->id, 'level_id' => $levelId]
                            );
                        }
                    }
                    $studentMedical = \App\models\StudentMedical::updateOrCreate(
                                    ['parent_id' => $parent->id], ['parent_id' => $parent->id, 'name' => $requestData['name'], 'email' => $requestData['email'], 'phone' => $requestData['phone'], 'address' => $requestData['sm_address']]
                    );
                    if (isset($requestData['student']['id']) && is_array($requestData['student']['id'])) {
                        foreach ($requestData['student']['id'] as $key => $studentId) {
                            $student = \App\models\Student::updateOrCreate(
                                            ['parent_id' => $parent->id, 'id' => $studentId], [ 'parent_id' => $parent->id, 'first_name' => $requestData['student']['first_name'][$key], 'last_name' => $requestData['student']['last_name'][$key],
                                        'birth_date' => date('Y-m-d H:i:s', strtotime($requestData['student']['birth_date'][$key])), 'apply_level' => $requestData['student']['apply_level'][$key], 'updated_at' => date('Y-m-d H:i:s')]
                            );
                        }
                    }
                    if (isset($requestData['emg']['id']) && is_array($requestData['emg']['id'])) {
                        foreach ($requestData['emg']['id'] as $key => $emgId) {
                            $studentRelationContact = \App\models\StudentRelationContact::updateOrCreate(
                                            ['parent_id' => $parent->id, 'id' => $emgId], ['parent_id' => $parent->id, 'first_name' => $requestData['emg']['first_name'][$key], 'last_name' => $requestData['emg']['last_name'][$key],
                                        'mobile' => $requestData['emg']['mobile'][$key], 'relation' => $requestData['emg']['relation'][$key]]
                            );
                        }
                    }
                    //  Update Parent
                    $parnt = \App\models\Parents::updateOrCreate(
                                    ['id' => $parent->id], [ 'user_id' => $user->ID, 'mother_first_name' => $requestData['mother_first_name'], 'mother_last_name' => $requestData['mother_last_name'],
                                'fother_first_name' => $requestData['fother_first_name'], 'fother_last_name' => $requestData['fother_last_name'], 'parent_email' => $requestData['parent_email'],
                                'address' => $requestData['address'], 'city' => $requestData['city'], 'state' => $requestData['state'], 'zip' => $requestData['zip'],
                                'home_phone_number' => $requestData['home_phone_number'], 'mother_mobile_number' => $requestData['mother_mobile_number'], 'father_mobile_number' => $requestData['father_mobile_number'],
                                    ]
                    );
                }
            }
        } else {
            $user = \App\User::create(
                            [ 'user_login' => $requestData['user_email'], 'user_pass' => WpPassword::make($requestData['user_pass']),
                                'user_nicename' => $requestData['user_nicename'], 'user_email' => $requestData['user_email'], 'user_url' => '',
                                'user_registered' => date('Y-m-d H:i:s'), 'user_status' => 1, 'display_name' => $requestData['display_name'],
                                'user_type' => 'PARENTS'
                            ]
            );
            if ($user) {
                DB::table('wp_usermeta')->insert(['user_id' => $user->ID, 'meta_key' => 'show_admin_bar_front', 'meta_value' => 'false']);
                DB::table('wp_usermeta')->insert(['user_id' => $user->ID, 'meta_key' => 'wp_capabilities', 'meta_value' => 'a:1:{s:6:"parent";b:1;}']);
                $parent = \App\models\Parents::updateOrCreate(
                                ['id' => $parent->id], [ 'user_id' => $user->ID, 'mother_first_name' => $requestData['mother_first_name'], 'mother_last_name' => $requestData['mother_last_name'],
                            'fother_first_name' => $requestData['fother_first_name'], 'fother_last_name' => $requestData['fother_last_name'], 'parent_email' => $requestData['parent_email'],
                            'address' => $requestData['address'], 'city' => $requestData['city'], 'state' => $requestData['state'], 'zip' => $requestData['zip'],
                            'home_phone_number' => $requestData['home_phone_number'], 'mother_mobile_number' => $requestData['mother_mobile_number'], 'father_mobile_number' => $requestData['father_mobile_number'],
                                ]
                );
                if ($parent) {
                    if (isset($requestData['level_id']) && is_array($requestData['level_id'])) {
                        \App\models\ParentsLevel::where('parent_id', $parent->id)->delete();
                        foreach ($requestData['level_id']as $key => $levelId) {
                            $student = \App\models\ParentsLevel::create(
                                            ['parent_id' => $parent->id, 'level_id' => $levelId]
                            );
                        }
                    }
                    $studentMedical = \App\models\StudentMedical::updateOrCreate(
                                    ['parent_id' => $parent->id], ['parent_id' => $parent->id, 'name' => $requestData['name'], 'email' => $requestData['email'], 'phone' => $requestData['phone'], 'address' => $requestData['sm_address']]
                    );
                    if (isset($requestData['student']['id']) && is_array($requestData['student']['id'])) {
                        foreach ($requestData['student']['id'] as $key => $studentId) {
                            $student = \App\models\Student::updateOrCreate(
                                            ['parent_id' => $parent->id, 'id' => $studentId], [ 'parent_id' => $parent->id, 'first_name' => $requestData['student']['first_name'][$key], 'last_name' => $requestData['student']['last_name'][$key],
                                        'birth_date' => date('Y-m-d H:i:s', strtotime($requestData['student']['birth_date'][$key])), 'apply_level' => $requestData['student']['apply_level'][$key], 'updated_at' => date('Y-m-d H:i:s')]
                            );
                        }
                    }
                    if (isset($requestData['emg']['id']) && is_array($requestData['emg']['id'])) {
                        foreach ($requestData['emg']['id'] as $key => $emgId) {
                            $studentRelationContact = \App\models\StudentRelationContact::updateOrCreate(
                                            ['parent_id' => $parent->id, 'id' => $emgId], ['parent_id' => $parent->id, 'first_name' => $requestData['emg']['first_name'][$key], 'last_name' => $requestData['emg']['last_name'][$key],
                                        'mobile' => $requestData['emg']['mobile'][$key], 'relation' => $requestData['emg']['relation'][$key]]
                            );
                        }
                    }
                }
            }
        }



        return redirect('parent')->with('flash_message', 'Parent added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $parent = Parents::findOrFail($id);
        if (isset($parent->user_id) && !empty($parent->user_id)) {
            $user = \App\User::find($parent->user_id);
        } else {
            $user = new \App\User();
        }

        return view('admin.parent.show', compact('parent', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $parent = Parents::findOrFail($id);
        if (isset($parent->user_id) && !empty($parent->user_id)) {
            $user = \App\User::find($parent->user_id);
        }
        if (!$user) {
            $user = new \App\User();
            $user->user_login = $parent->parent_email;
            $user->user_email = $parent->parent_email;
            $user->user_nicename = $parent->fother_first_name . ' ' . $parent->fother_last_name;
            $user->display_name = $parent->fother_first_name . ' ' . $parent->fother_last_name;
        }

        $studentRelationContact = $parent->studentRelationContact;
        $student = $parent->student;
        $levels = $parent->levels;
        $studentMedical = $parent->studentMedical;

        return view('admin.parent.edit', compact('parent', 'user', 'studentRelationContact', 'student', 'studentMedical', 'levels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), [
                    //'user_email' => 'required|unique:wp_users|max:255',
                    // 'user_pass' => 'required',
                    'level_id' => 'required',
                    'display_name' => 'required',
                    'mother_first_name' => 'required',
                    'fother_first_name' => 'required',
                    'father_mobile_number' => 'required',
                    'transaction_id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('parent/' . $id . '/edit')
                            ->withErrors($validator)
                            ->withInput();
        }


        $parent = Parents::findOrFail($id);
        if ($parent) {
            $requestData = $request->all();
            $user = \App\User::updateOrCreate(
                            ['user_login' => $requestData['user_email']], [ 'user_login' => $requestData['user_email'],
                        'user_nicename' => $requestData['user_nicename'], 'user_email' => $requestData['user_email'], 'user_url' => '',
                        'user_registered' => date('Y-m-d H:i:s'), 'user_status' => 1, 'display_name' => $requestData['display_name'],
                        'user_type' => 'PARENTS'
                            ]
            );
            if ($user) {
                DB::table('wp_usermeta')->insert(['user_id' => $user->ID, 'meta_key' => 'show_admin_bar_front', 'meta_value' => 'false']);
                DB::table('wp_usermeta')->insert(['user_id' => $user->ID, 'meta_key' => 'wp_capabilities', 'meta_value' => 'a:1:{s:6:"parent";b:1;}']);
                if (isset($requestData['level_id']) && is_array($requestData['level_id'])) {
                    \App\models\ParentsLevel::where('parent_id', $parent->id)->delete();
                    foreach ($requestData['level_id']as $key => $levelId) {
                        $student = \App\models\ParentsLevel::create(
                                        ['parent_id' => $parent->id, 'level_id' => $levelId]
                        );
                    }
                }
                $studentMedical = \App\models\StudentMedical::updateOrCreate(
                                ['parent_id' => $parent->id], ['parent_id' => $parent->id, 'name' => $requestData['name'], 'email' => $requestData['email'], 'phone' => $requestData['phone'], 'address' => $requestData['sm_address']]
                );
                if (isset($requestData['student']['id']) && is_array($requestData['student']['id'])) {
                    foreach ($requestData['student']['id'] as $key => $studentId) {
                        $student = \App\models\Student::updateOrCreate(
                                        ['parent_id' => $parent->id, 'id' => $studentId], [ 'parent_id' => $parent->id, 'first_name' => $requestData['student']['first_name'][$key], 'last_name' => $requestData['student']['last_name'][$key],
                                    'birth_date' => date('Y-m-d H:i:s', strtotime($requestData['student']['birth_date'][$key])), 'apply_level' => $requestData['student']['apply_level'][$key], 'updated_at' => date('Y-m-d H:i:s')]
                        );
                    }
                }
                if (isset($requestData['emg']['id']) && is_array($requestData['emg']['id'])) {
                    foreach ($requestData['emg']['id'] as $key => $emgId) {
                        $studentRelationContact = \App\models\StudentRelationContact::updateOrCreate(
                                        ['parent_id' => $parent->id, 'id' => $emgId], ['parent_id' => $parent->id, 'first_name' => $requestData['emg']['first_name'][$key], 'last_name' => $requestData['emg']['last_name'][$key],
                                    'mobile' => $requestData['emg']['mobile'][$key], 'relation' => $requestData['emg']['relation'][$key]]
                        );
                    }
                }
                //  Update Parent
                $parnt = \App\models\Parents::updateOrCreate(
                                ['id' => $parent->id], [ 'user_id' => $user->ID, 'mother_first_name' => $requestData['mother_first_name'], 'mother_last_name' => $requestData['mother_last_name'],
                            'fother_first_name' => $requestData['fother_first_name'], 'fother_last_name' => $requestData['fother_last_name'], 'parent_email' => $requestData['parent_email'],
                            'address' => $requestData['address'], 'city' => $requestData['city'], 'state' => $requestData['state'], 'zip' => $requestData['zip'],
                            'home_phone_number' => $requestData['home_phone_number'], 'mother_mobile_number' => $requestData['mother_mobile_number'], 'father_mobile_number' => $requestData['father_mobile_number'],
                                ]
                );
            }
        }


        return redirect('parent')->with('flash_message', 'Parent updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        \App\models\ParentsLevel::where('parent_id', $id)->delete();
        \App\models\StudentRelationContact::where('parent_id', $id)->delete();
        \App\models\Student::where('parent_id', $id)->delete();
        \App\models\StudentMedical::where('parent_id', $id)->delete();
        Parents::destroy($id);

        return redirect('parent')->with('flash_message', 'Parent deleted!');
    }

}
