<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\models\Level;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LevelController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $level = Level::where('name', 'LIKE', "%$keyword%")
                            ->orWhere('slug', 'LIKE', "%$keyword%")
                            ->latest()->paginate($perPage);
        } else {
            $level = Level::latest()->paginate($perPage);
        }

        return view('admin.level.index', compact('level'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.level.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'name' => 'required|unique:levels|max:255',
                    'slug' => 'required|unique:levels|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('level/create')
                            ->withErrors($validator)
                            ->withInput();
        }
        $requestData = $request->all();

        Level::create($requestData);

        return redirect('level')->with('flash_message', 'Level added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $level = Level::findOrFail($id);

        return view('admin.level.show', compact('level'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $level = Level::findOrFail($id);

        return view('admin.level.edit', compact('level'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {
        $requestData = $request->all();

        $level = Level::findOrFail($id);
        $validator = Validator::make($request->all(), [

                    'name' => 'required|unique:levels,name,' . $level->id . '|max:255',
                    'slug' => 'required|unique:levels,slug,' . $level->id . '|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('level/' . $id . '/edit')
                            ->withErrors($validator)
                            ->withInput();
        }

        $level->update($requestData);

        return redirect('level')->with('flash_message', 'Level updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        Level::destroy($id);

        return redirect('level')->with('flash_message', 'Level deleted!');
    }

}
