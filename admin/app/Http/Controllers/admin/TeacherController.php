<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\models\Teacher;
use Illuminate\Http\Request;
use MikeMcLin\WpPassword\Facades\WpPassword;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class TeacherController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $teacher = Teacher::where('display_name', 'LIKE', "%$keyword%")
                            ->orWhere('user_nicename', 'LIKE', "%$keyword%")
                            ->orWhere('user_email', 'LIKE', "%$keyword%")
                            ->teacher()
                            ->latest('user_registered')->paginate($perPage);
        } else {
            $teacher = Teacher::teacher()->latest('user_registered')->paginate($perPage);
        }

        return view('admin.teacher.index', compact('teacher'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.teacher.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $customMessages = [
            'user_email.required' => 'Email is required.',
            'user_email.unique' => 'The email has already been taken.',
            'level_id.required' => 'Levels is required.',
            'display_name.required' => 'Name is required',
            'user_pass.required' => 'Password is required',
        ];

        $validator = Validator::make($request->all(), [
                    'user_email' => 'required|unique:wp_users|max:255',
                    'user_pass' => 'required',
                    'level_id' => 'required',
                    'display_name' => 'required',
                        ], $customMessages);

        if ($validator->fails()) {
            return redirect('teacher/create')
                            ->withErrors($validator)
                            ->withInput();
        }
        $requestData = $request->all();
        $requestData['user_login'] = $request->user_email;
        $requestData['user_nicename'] = $request->display_name;
        $requestData['user_pass'] = WpPassword::make($request->user_pass);
        $requestData['user_type'] = 'TEACHER';
        $requestData['user_registered'] = date('Y-m-d H:i:s');

        $teacher = Teacher::create($requestData);
        if ($teacher) {
            DB::table('wp_usermeta')->insert(['user_id' => $teacher->ID, 'meta_key' => 'show_admin_bar_front', 'meta_value' => 'false']);
            DB::table('wp_usermeta')->insert(['user_id' => $teacher->ID, 'meta_key' => 'wp_capabilities', 'meta_value' => 'a:1:{s:7:"teacher";b:1;}']);


            if (isset($requestData['level_id']) && is_array($requestData['level_id'])) {
                \App\models\UsersLevel::where('user_id', $teacher->ID)->delete();
                foreach ($requestData['level_id']as $key => $levelId) {
                    $student = \App\models\UsersLevel::create(
                                    ['user_id' => $teacher->ID, 'level_id' => $levelId]
                    );
                }
            }
        }
        return redirect('teacher')->with('flash_message', 'Teacher added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $teacher = Teacher::findOrFail($id);

        return view('admin.teacher.show', compact('teacher'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $teacher = Teacher::findOrFail($id);

        return view('admin.teacher.edit', compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {
        $customMessages = [
            'user_email.required' => 'Email is required.',
            'user_email.unique' => 'The email has already been taken.',
            'level_id.required' => 'Levels is required.',
            'display_name.required' => 'Name is required',
        ];
        $validator = Validator::make($request->all(), [
                    'user_email' => 'required|unique:wp_users,user_email,' . $id . '|max:255',
                    'level_id' => 'required',
                    'display_name' => 'required',
                        ], $customMessages);

        if ($validator->fails()) {
            return redirect('teacher/' . $id . '/edit')
                            ->withErrors($validator)
                            ->withInput();
        }
        $requestData = $request->all();
        $requestData['user_nicename'] = $request->display_name;
        $requestData['user_type'] = 'TEACHER';
        $teacher = Teacher::findOrFail($id);
        $teacher->update($requestData);

        if (isset($requestData['level_id']) && is_array($requestData['level_id'])) {
            \App\models\UsersLevel::where('user_id', $teacher->ID)->delete();
            foreach ($requestData['level_id']as $key => $levelId) {
                $student = \App\models\UsersLevel::create(
                                ['user_id' => $teacher->ID, 'level_id' => $levelId]
                );
            }
        }
        return redirect('teacher')->with('flash_message', 'Teacher updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        \App\models\UsersLevel::where('user_id', $id)->delete();
        Teacher::destroy($id);

        return redirect('teacher')->with('flash_message', 'Teacher deleted!');
    }

    public function teacherLevel($id) {
        $levels = DB::table("levels")
                ->join('wp_users_level', 'wp_users_level.level_id', '=', 'levels.id')
                ->where("wp_users_level.user_id", $id)
                ->pluck("levels.name", "levels.id");
        return json_encode($levels);
    }

}
