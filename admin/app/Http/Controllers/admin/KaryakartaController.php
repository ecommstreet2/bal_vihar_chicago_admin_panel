<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\models\Karyakarta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class KaryakartaController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $karyakarta = Karyakarta::where('name', 'LIKE', "%$keyword%")
                            ->orWhere('education', 'LIKE', "%$keyword%")
                            ->orWhere('designation', 'LIKE', "%$keyword%")
                            ->orWhere('description', 'LIKE', "%$keyword%")
                            ->orWhere('phone', 'LIKE', "%$keyword%")
                            ->orWhere('address', 'LIKE', "%$keyword%")
                            ->orWhere('email', 'LIKE', "%$keyword%")
                            ->orWhere('image', 'LIKE', "%$keyword%")
                            ->orWhere('status', 'LIKE', "%$keyword%")
                            ->latest()->paginate($perPage);
        } else {
            $karyakarta = Karyakarta::latest()->paginate($perPage);
        }

        return view('admin.karyakarta.index', compact('karyakarta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.karyakarta.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'email' => 'unique:wp_karyakarta',
                    'name' => 'required',
//                     'education' => 'required', 'designation' => 'required', 'description' => 'required',
//                    'phone' => 'required', 'address' => 'required',
                    'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {
            return redirect('karyakarta/create')
                            ->withErrors($validator)
                            ->withInput();
        }
        $requestData = $request->all();
        if (isset($request->image) && !empty($request->image)) {
            $imageName = time() . '.' . $request->image->extension();
            $requestData['image'] = asset('uploads/' . $imageName);
            $request->image->move(public_path('uploads'), $imageName);
        }
        Karyakarta::create($requestData);

        return redirect('karyakarta')->with('flash_message', 'Karyakarta added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $karyakarta = Karyakarta::findOrFail($id);

        return view('admin.karyakarta.show', compact('karyakarta'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $karyakarta = Karyakarta::findOrFail($id);

        return view('admin.karyakarta.edit', compact('karyakarta'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {

        $requestData = $request->all();

        $karyakarta = Karyakarta::findOrFail($id);
        $validator = Validator::make($request->all(), [

                    'email' => 'unique:wp_karyakarta,email,' . $karyakarta->id . '|max:255',
                    'name' => 'required',
//            'education' => 'required', 'designation' => 'required', 'description' => 'required',
//                    'phone' => 'required', 'address' => 'required', 
                    'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {
            return redirect('karyakarta/' . $id . '/edit')
                            ->withErrors($validator)
                            ->withInput();
        }
        if (isset($request->image) && !empty($request->image)) {
            if (file_exists(public_path('uploads') . '/' . $karyakarta->image)) {
                File::delete(public_path('uploads') . '/' . $karyakarta->image);
            }
            $imageName = time() . '.' . $request->image->extension();
            $requestData['image'] = asset('uploads/' . $imageName);
            $request->image->move(public_path('uploads'), $imageName);
        } else {
            $requestData['image'] = $karyakarta->image;
        }
        $karyakarta->update($requestData);

        return redirect('karyakarta')->with('flash_message', 'Karyakarta updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        Karyakarta::destroy($id);

        return redirect('karyakarta')->with('flash_message', 'Karyakarta deleted!');
    }

}
