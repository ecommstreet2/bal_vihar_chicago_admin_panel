<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\models\HomePageSlider;
use App\models\NewsMeta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HomePageSliderController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $homePageSliders = HomePageSlider::where('post_type', 'slider')
                            ->where('post_title', 'LIKE', "%$keyword%")
                            
                            ->orWhere('post_content', 'LIKE', "%$keyword%")
                            ->latest('id')->paginate($perPage);
        } else {
            $homePageSliders = HomePageSlider::latest('id')->paginate($perPage);
        }

        return view('admin.home-page-slider.index', compact('homePageSliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.home-page-slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $requestData = $request->all();

        $createdAt = time();
        $validator = Validator::make($request->all(), [

                    'post_title' => 'required', 'post_content' => 'required', 'post_date' => 'required', 'destination_link' => 'required|url',
                    'slider_image' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {
            return redirect('home-page-slider/create')
                            ->withErrors($validator)
                            ->withInput();
        }

        if (isset($request->slider_image) && !empty($request->slider_image)) {
            $imageName = time() . '.' . $request->slider_image->extension();
            $requestData['slider_image'] = asset('uploads/' . $imageName);
            $request->slider_image->move(public_path('uploads'), $imageName);
        }
        $requestData['post_date'] = date('Y-m-d', strtotime($request->post_date));


        $homePageSlider = HomePageSlider::create($requestData);

        return redirect('home-page-slider')->with('flash_message', 'Slider added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $homePageSlider = HomePageSlider::findOrFail($id);

        return view('admin.home-page-slider.show', compact('homePageSlider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $homePageSlider = HomePageSlider::findOrFail($id);

        return view('admin.home-page-slider.edit', compact('homePageSlider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {

        $requestData = $request->all();

        $homePageSlider = HomePageSlider::findOrFail($id);
        $createdAt = time();
        $validator = Validator::make($request->all(), [

                    'post_title' => 'required', 'post_content' => 'required', 'post_date' => 'required', 'destination_link' => 'required|url',
                    'slider_image' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {

            return redirect('home-page-slider/' . $id . '/edit')
                            ->withErrors($validator)
                            ->withInput();
        }
        if (isset($request->slider_image) && !empty($request->slider_image)) {
            if (file_exists(public_path('uploads') . '/' . $homePageSlider->slider_image)) {
                File::delete(public_path('uploads') . '/' . $homePageSlider->slider_image);
            }
            $imageName = time() . '.' . $request->slider_image->extension();
            $requestData['slider_image'] = asset('uploads/' . $imageName);
            $request->slider_image->move(public_path('uploads'), $imageName);
        } else {
            $requestData['slider_image'] = $homePageSlider->slider_image;
        }

        $requestData['post_date'] = date('Y-m-d', strtotime($request->post_date));
   

        $homePageSlider->update($requestData);

        return redirect('home-page-slider')->with('flash_message', 'Slider updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
       
        HomePageSlider::destroy($id);

        return redirect('home-page-slider')->with('flash_message', 'Slider deleted!');
    }

}
