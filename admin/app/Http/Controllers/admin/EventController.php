<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\models\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class EventController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $event = Event::where('event_name', 'LIKE', "%$keyword%")
                            ->orWhere('event_description', 'LIKE', "%$keyword%")
                            ->orWhere('event_location', 'LIKE', "%$keyword%")
                            ->orWhere('updated_at', 'LIKE', "%$keyword%")
                            ->orWhere('status', 'LIKE', "%$keyword%")
                            ->latest()->paginate($perPage);
        } else {
            $event = Event::latest()->paginate($perPage);
        }

        return view('admin.event.index', compact('event'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.event.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {

        $validator = Validator::make($request->all(), [
                    'event_name' => 'required',
                    'event_description' => 'required',
                    'event_location' => 'required',
                    'event_start_date' => 'required|date|date_format:m/d/Y',
                    'event_start_time' => 'required|date_format:H:i:s',
                    'event_end_date' => 'required|date|date_format:m/d/Y|after_or_equal:event_start_date',
                    'event_end_time' => 'required|date_format:H:i:s',
                    'event_image' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {
            return redirect('event/create')
                            ->withErrors($validator)
                            ->withInput();
        }
        $requestData = $request->all();
        if (isset($request->event_image) && !empty($request->event_image)) {
            $imageName = time() . '.' . $request->event_image->extension();
            $requestData['event_image'] = asset('uploads/' . $imageName);
            $request->event_image->move(public_path('uploads'), $imageName);
        }
        $requestData['event_start_date'] = date('Y-m-d H:i:s', strtotime($request->event_start_date));
        $requestData['event_end_date'] = date('Y-m-d H:i:s', strtotime($request->event_end_date));
        Event::create($requestData);

        return redirect('event')->with('flash_message', 'Event added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $event = Event::findOrFail($id);

        return view('admin.event.show', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $event = Event::findOrFail($id);

        return view('admin.event.edit', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {

        $requestData = $request->all();

        $event = Event::findOrFail($id);

        $validator = Validator::make($request->all(), [
                    'event_name' => 'required',
                    'event_description' => 'required',
                    'event_location' => 'required',
                    'event_start_date' => 'required|date|date_format:m/d/Y',
                    'event_start_time' => 'required|date_format:H:i:s',
                    'event_end_date' => 'required|date|date_format:m/d/Y|after_or_equal:event_start_date',
                    'event_end_time' => 'required|date_format:H:i:s',
                    'event_image' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {
            return redirect('event/' . $id . '/edit')
                            ->withErrors($validator)
                            ->withInput();
        }

        if (isset($request->event_image) && !empty($request->event_image)) {
            $imageName = time() . '.' . $request->event_image->extension();
            $requestData['event_image'] = asset('uploads/' . $imageName);
            $request->event_image->move(public_path('uploads'), $imageName);
        }
        $requestData['event_start_date'] = date('Y-m-d H:i:s', strtotime($request->event_start_date));
        $requestData['event_end_date'] = date('Y-m-d H:i:s', strtotime($request->event_end_date));



        $event->update($requestData);

        return redirect('event')->with('flash_message', 'Event updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        Event::destroy($id);

        return redirect('event')->with('flash_message', 'Event deleted!');
    }

}
