<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\models\News;
use App\models\NewsMeta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $news = News::where('post_type', 'news')
                            ->where('news_title', 'LIKE', "%$keyword%")
                            ->orWhere('news_image', 'LIKE', "%$keyword%")
                            ->orWhere('status', 'LIKE', "%$keyword%")
                            ->orWhere('news_date', 'LIKE', "%$keyword%")
                            ->orWhere('news_dscription', 'LIKE', "%$keyword%")
                            ->latest('id')->paginate($perPage);
        } else {
            $news = News::where('post_type', 'news')->latest('id')->paginate($perPage);
        }

        return view('admin.news.index', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $requestData = $request->all();

        $createdAt = time();
        $validator = Validator::make($request->all(), [

                    'post_title' => 'required', 'post_content' => 'required', 'post_date' => 'required',
                    'news_image' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {
            return redirect('news/create')
                            ->withErrors($validator)
                            ->withInput();
        }

        if (isset($request->news_image) && !empty($request->news_image)) {
            $imageName = time() . '.' . $request->news_image->extension();
            $requestData['guid'] = asset('uploads/' . $imageName);
            $request->news_image->move(public_path('uploads'), $imageName);
        }
        $requestData['post_date'] = date('Y-m-d H:i:s', strtotime($request->post_date));
        $requestData['post_date_gmt'] = gmdate('Y-m-d H:i:s', strtotime($request->post_date));
        $requestData['post_modified'] = date('Y-m-d H:i:s', $createdAt);
        $requestData['post_modified_gmt'] = gmdate('Y-m-d H:i:s', $createdAt);


        $requestData['post_author'] = Auth::user()->ID;
        $requestData['post_status'] = 'publish';
        $requestData['comment_status'] = 'open';
        $requestData['ping_status'] = 'closed';
        $requestData['post_name'] = Str::slug($request->post_title, '-');
        $requestData['post_type'] = 'news';
        $requestData['post_excerpt'] = '';
        $requestData['to_ping'] = '';
        $requestData['pinged'] = '';
        $requestData['post_content_filtered'] = '';





        $news = News::create($requestData);
        if ($news) {
//            DB::table('wp_posts')
//                    ->where('ID', $news->ID)
//                    ->update(['guid' => 1]);
            DB::table('wp_postmeta')->insert(['post_id' => $news->ID, 'meta_key' => '_edit_lock', 'meta_value' => time() . ':1']);
            DB::table('wp_postmeta')->insert(['post_id' => $news->ID, 'meta_key' => '_edit_last', 'meta_value' => 1]);
            DB::table('wp_postmeta')->insert(['post_id' => $news->ID, 'meta_key' => '_thumbnail_id', 'meta_value' => '']);
            DB::table('wp_postmeta')->insert(['post_id' => $news->ID, 'meta_key' => 'slide_template', 'meta_value' => '']);
            DB::table('wp_postmeta')->insert(['post_id' => $news->ID, 'meta_key' => 'rs_page_bg_color', 'meta_value' => '']);
        }

        return redirect('news')->with('flash_message', 'News added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $news = News::where('post_type', 'news')->findOrFail($id);

        return view('admin.news.show', compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $news = News::findOrFail($id);

        return view('admin.news.edit', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {

        $requestData = $request->all();

        $news = News::findOrFail($id);
        $createdAt = time();
        $validator = Validator::make($request->all(), [

                    'post_title' => 'required', 'post_content' => 'required', 'post_date' => 'required',
                    'news_image' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {

            return redirect('news/' . $id . '/edit')
                            ->withErrors($validator)
                            ->withInput();
        }
        if (isset($request->news_image) && !empty($request->news_image)) {
            if (file_exists(public_path('uploads') . '/' . $news->news_image)) {
                File::delete(public_path('uploads') . '/' . $news->news_image);
            }
            $imageName = time() . '.' . $request->news_image->extension();
            $requestData['guid'] = asset('uploads/' . $imageName);
            $request->news_image->move(public_path('uploads'), $imageName);
        } else {
            $requestData['news_image'] = $news->news_image;
        }

        $requestData['post_date'] = date('Y-m-d H:i:s', strtotime($request->post_date));
        $requestData['post_date_gmt'] = gmdate('Y-m-d H:i:s', strtotime($request->post_date));
        $requestData['post_modified'] = date('Y-m-d H:i:s', $createdAt);
        $requestData['post_modified_gmt'] = gmdate('Y-m-d H:i:s', $createdAt);

        $requestData['post_status'] = 'publish';
        $requestData['post_name'] = Str::slug($request->post_title, '-');
        $requestData['post_type'] = 'news';

        $news->update($requestData);

        return redirect('news')->with('flash_message', 'News updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        DB::table('wp_postmeta')->where('post_id', $id)->delete();
        News::destroy($id);

        return redirect('news')->with('flash_message', 'News deleted!');
    }

}
