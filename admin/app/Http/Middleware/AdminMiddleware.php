<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use App\User;

class AdminMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

        if ($request->user() && $request->user()->user_type != User::ADMIN_ROLE) {
            $role = 'Admin';
            return new Response(view('unauthorized', compact('role')));
        }
        return $next($request);
    }

}
