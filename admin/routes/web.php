<?php

use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */



Auth::routes();

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', 'HomeController@index')->name('home');
Route::group(array('prefix' => '', 'middleware' => ['auth', 'admin']), function () {

    Route::resource('level', 'admin\LevelController');
    Route::resource('event', 'admin\\EventController');
    Route::resource('parent', 'admin\ParentController');
    Route::resource('teacher', 'admin\TeacherController');
    Route::resource('karyakarta', 'admin\KaryakartaController');
    Route::resource('news', 'admin\NewsController');
    Route::resource('transaction', 'admin\TransactionController');
    Route::resource('gallery', 'admin\GalleryController');
    Route::resource('photo-gallery', 'admin\PhotoGalleryController');
    Route::resource('video-gallery', 'admin\VideoGalleryController');
    Route::post('photo-galleries/create', array('as' => 'photo-galleries.create', 'uses' => 'admin\PhotoGalleryController@createGallery'));
    Route::post('video-galleries/create', array('as' => 'video-galleries.create', 'uses' => 'admin\VideoGalleryController@createGallery'));
    Route::resource('home-page-slider', 'admin\HomePageSliderController');
    Route::resource('syllabus-calender', 'admin\SyllabusCalenderController');
    Route::resource('academic-calender', 'admin\AcademicCalenderController');
});
Route::group(array('prefix' => '', 'middleware' => ['auth']), function () {

    Route::resource('library', 'admin\LibraryController');
    Route::resource('resources', 'admin\ResourcesController');
    Route::resource('assignment', 'admin\AssignmentController');
    Route::resource('latest-updates', 'admin\LatestUpdatesController');
    Route::get('teacherLevel/{id}', array('as' => 'teacher.level', 'uses' => 'admin\TeacherController@teacherLevel'));
});

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset.token');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');



